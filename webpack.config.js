module.exports = {
    mode: 'development',
    entry: {
      mb: './src/js/medblocks.js',
    },
    devtool: 'inline-source-map',
   devServer: {
     contentBase: './dist',
     port: 9000
   },
}