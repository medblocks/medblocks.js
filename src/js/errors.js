class HttpError extends Error {
    constructor(response) {
        super()
        this.name = 'HttpError'
        this.message = 'Invalid Response status: ' + response.status
    }
}
class PermissionError extends Error {
    constructor() {
        super()
        this.name = 'PermissionError'
        this.message = 'Invalid Permissions for the requested medblock'
    }
}
class LoginError extends Error {
    constructor(){
    super()
    this.name = 'LoginError'
    this.message = 'Please make sure you are logged in to continue.'
    }
}
export {HttpError,PermissionError,LoginError}