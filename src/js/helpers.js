
import {HttpError} from './errors.js'
var helpers = {}
helpers.base64ToArrayBuffer = (base64) => {
    var binary_string = window.atob(base64)
    var len = binary_string.length
    var bytes = new Uint8Array(len)
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i)
    }
    return bytes.buffer
}

helpers.arrayToBase64 = (array) => {
    return btoa(String.fromCharCode.apply(null, new Uint8Array(array)))
}
helpers.convertStringToArrayBuffer = (str) => { //needed for importkey I/P
    var encoder = new TextEncoder("utf-8")
    return encoder.encode(str)
}
helpers.convertArrayBuffertoString = (buffer) => {
    var decoder = new TextDecoder("utf-8")
    return decoder.decode(buffer)
}
// Calculates hexadecimal string representation of array buffer
helpers.hexString = (buffer) => {
    const byteArray = new Uint8Array(buffer)
    const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16)
        const paddedHexCode = hexCode.padStart(2, '0')
        return paddedHexCode
    })
    return hexCodes.join('')
}

helpers.removeLines = (pem) => {
    var lines = pem.split('\n')
    var encodedString = ''
    for (var i = 1; i < (lines.length - 1); i++) {
        encodedString += lines[i].trim()
    }
    return encodedString
}

helpers.spkiToPEM = (keyBuffer) => {
    // var keydataB64S = helpers.converArrayBuffertoString(keyBuffer) 
    // var keydatab64 = this.b64EncodeUnicode(keydataB64S)
    // var keydataB64Pem = this.formatAsPem(keydataB	64)//add new lines and split into 64
    var keydataB64 = helpers.arrayToBase64(keyBuffer)
    var keydataB64Pem = helpers.formatAsPem(keydataB64)
    return keydataB64Pem
}

helpers.formatAsPem = (str) => {
    var finalString = '-----BEGIN PUBLIC KEY-----\n'

    while (str.length > 0) {
        finalString += str.substring(0, 64) + '\n'
        str = str.substring(64)
    }

    finalString = finalString + "-----END PUBLIC KEY-----"

    return finalString
}
helpers.handleFetchError = (response) => {
    if (response.status !== 200 && response.status !== 202) {
        //console.error("Response Object: ")
        //console.error("Response object: "+JSON.stringify(response))
        throw new HttpError(response)
    }
}
export default helpers