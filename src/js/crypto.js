/**
    * SECTION: crypto.js
    */

/*
* Contains all necessary cryptographic functions imlemented through webcrypto
*/
import helpers from './helpers.js'
var crypt = {}
//const iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105]) 

crypt.generate_RSA_Keypair = async () => {
    let generatedKey = await crypto.subtle.generateKey
        (
            //algorithmIdentifier
            {
                "name": "RSA-OAEP",
                modulusLength: 2048,
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash:
                {
                    name: 'SHA-256'
                }

            },
            //boolean extractible true so exportKey can be called
            true,
            //keyUsages values set to encrypt,decrypt
            ["encrypt", "decrypt"]
        )
        .then(function (keypair) {
            return keypair
        })
        .catch((e) => { console.error("Error occured during RSA-OEAP key generation Message :" + e) })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_RSASSA_KeyPair = async () => {
    var generatedKey = await window.crypto.subtle.generateKey
        (
            {
                name: "RSASSA-PKCS1-v1_5",
                modulusLength: 2048, //can be 1024, 2048, or 4096
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
            },
            true, //whether the key is extractable (i.e. can be used in exportKey)
            ["sign", "verify"] //can be any combination of "sign" and "verify"
        )
        .then(function (keypair) {
            return keypair
        })
        .catch(function (e) {
            console.error(e)
        })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_AES_Key = async () => {
    var key = await window.crypto.subtle.generateKey(
        {
            name: "AES-GCM",
            length: 256, //can be  128, 192, or 256
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns a key object
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}

crypt.getPkcs8 = async (privateKeyCrypto) => {
    var pk_pkcs8 = await window.crypto.subtle.exportKey(//returns array buffer
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata //arrayBuffer of the private key
        })
        .catch(function (e) {
            console.error(e)
        })
    return pk_pkcs8
}
crypt.getPem = async (publicKeyCrypto) => {
    var spki = await window.crypto.subtle.exportKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    let pem = helpers.spkiToPEM(spki)
    return pem
}
crypt.getPrivateKeyFromBuffer = async (priavteKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return privateKeyCrypto
}
crypt.getSPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var sPrivateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["sign"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return sPrivateKeyCrypto
}
crypt.getEPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error("error decrypting from decrypted buffer" + e)
        })
    return privateKeyCrypto
}
crypt.getEPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var publicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,//pass arraybuffer of the public key
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch((e) => {
            console.error(e, e.stack)
        })
    return publicKeyCrypto
}
crypt.getSPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var ePublicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["verify"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return ePublicKeyCrypto
}
crypt.encryptAesKey = async (aesKeyBuffer, publicKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var encryptedAesKey = await window.crypto.subtle.encrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        publicKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedAesKey//encrypt aes key with public key 
}
crypt.decryptAesKey = async (aesKeyBuffer, privateKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var decryptedAesKey = await window.crypto.subtle.decrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        privateKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to decrypt
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedAesKey//encrypt aes key with public key 
}
crypt.decrypt_AES = async (privateKeyString, password) => {
    var iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105])
    var key = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        helpers.convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var enkey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: helpers.convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        key, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    //step 2: use key to decrypt
    var decrypted_data = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        enkey, //from generateKey or importKey above
        //helpers.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        privateKeyString// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decrypted_data
}
crypt.encryptFileAes = async (fileBuffer, aesKey, iv) => {
    let encryptedFileBuffer = await window.crypto.subtle.encrypt(
        {
            name: "AES-GCM",

            //Don't re-use initialization vectors!
            //Always generate a new iv every time your encrypt!
            //Recommended to use 12 bytes length
            //iv: window.crypto.getRandomValues(new Uint8Array(12)),
            iv: iv,
            //Tag length (optional)
            tagLength: 128, //can be 32, 64, 96, 104, 112, 120 or 128 (default)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedFileBuffer
}
crypt.decryptFileAes = async (fileBuffer, aesKey, iv) => {
    let decryptedFileBuffer = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of the data
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedFileBuffer
}
crypt.getSignature = async (buffer) => {
    // if (Window.sPrivateKey== undefined){
    // 	this.getPrivateKey(emailId)
    // }
    let signature = await window.crypto.subtle.sign(
        {
            name: "RSASSA-PKCS1-v1_5",
        },
        Window.sPrivateKey, //from generateKey or importKey above
        buffer//ArrayBuffer of data you want to sign
    )
        .then(function (signature) {
            //returns an ArrayBuffer containing the signature
            return signature
        })
        .catch(function (e) {
            console.error(e)
        })
    return signature
}
crypt.exportAes = async (aesKey) => {
    let key = await window.crypto.subtle.exportKey(
        "raw", //can be "jwk" or "raw"
        aesKey //extractable must be true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.importAes = async (aesKeyBuffer) => {
    let key = await window.crypto.subtle.importKey(
        "raw", //can be "jwk" or "raw"
        aesKeyBuffer,
        {   //this is the algorithm options
            name: "AES-GCM",
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns the symmetric key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.encryptkey = async (password, keyBuffer) => {
    //create Initialization vector for aes key
    let iv = window.crypto.getRandomValues(new Uint8Array(16))
    //create a key from user's password
    let passwordKey = await window.crypto.subtle.importKey(
        "raw",
        helpers.convertStringToArrayBuffer(password),
        { "name": "PBKDF2" },
        false,
        ["deriveKey"]
    )
    let encryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: helpers.convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }
        },
        passwordKey,
        {
            "name": "AES-GCM",
            "length": 128
        },
        true,
        ["encrypt", "decrypt"]
    )
    let encryptedKeyData = await window.crypto.subtle.encrypt
        ({
            name: "AES-GCM",
            iv: iv
        },
            encryptionKey,
            keyBuffer
        )
    //let encryptedKeyBuffer = new Uint8Array(encryptedKeyData)
    return {
        encryptedKeyBuffer: encryptedKeyData,
        iv: iv
    }
}
crypt.decryptKey = async (password, keyBuffer, iv) => {
    //console.log(iv) 
    //console.log(keyBuffer)
    //console.log(password)
    //returns binary stream of private key
    var passwordKey = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        helpers.convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var decryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: helpers.convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        passwordKey, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        });
    //step 2: use key to decrypt
    var decryptedKeyData = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        decryptionKey, //from generateKey or importKey above
        //this.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        keyBuffer// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            //console.log("key decrypted: "+ decrypted)
            return decrypted
        })
        .catch(function (e) {
            console.error("Erroer while decrypting private key: " + e)
        })
    //console.log(decryptedKeyData)
    return decryptedKeyData
}

export default crypt