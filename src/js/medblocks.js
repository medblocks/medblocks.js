import asset from './assets.js';
import helpers from './helpers.js'
import crypt from './crypto.js'
import {PermissionError,HttpError} from './errors.js';
(function () {
    /**asset
     * SECTION: medblocks.js
     */
    
    /*
    * Main Medblocks class
    */

    class Medblocks {
        constructor(localNodeIP = "3.95.200.13",localNodePort = "8080",localIpfsIp = "3.95.200.13",localIpfsPort = "5001") {
            this.localNodeIP = localNodeIP, 
            this.localNodePort = localNodePort,
            this.localIpfsIp = localIpfsIp,
            this.localIpfsPort = localIpfsPort
        }
        async addPermission(hash,senderEmailId,receiverEmailId,nodeIp = this.localNodeIP,port = this.localNodePort)
        {
            
            //1: Get Block from blockchain and check if the loggedin user is the owner
            let block = await this.getBlock(hash,nodeIp,port)
            //2:Decrypt the receiverKey to get aes key
            let receiverKeyS
            for (let index=0 ; index < block.permissions.length ; index++)
            {
                if(block.permissions[index].receiverEmailId== senderEmailId){
                    receiverKeyS= block.permissions[index].receiverKey
                }
            }
            try
            {
            if (receiverKeyS==undefined)
            {   
                throw new PermissionError()    
            }
            }
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            let receiverKeyB = helpers.base64ToArrayBuffer(receiverKeyS)
            let aesKeyBuffer = await crypt.decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await crypt.importAes(aesKeyBuffer)
            //let receiverKey = block.permissions[0]["receiverKey"] works
            let receiverKeyBuffer = (await crypt.encryptAesKey(await crypt.exportAes(aesKey), await this.getPublicKey(receiverEmailId,nodeIp,port)))
            const url = "http://"+nodeIp+":"+port+"/block/permissions" 
            let data = JSON.stringify({
                ipfsHash: hash,
                senderEmailId: senderEmailId,
                permissions:  [
                    { receiverEmailId: receiverEmailId, receiverKey: helpers.arrayToBase64(receiverKeyBuffer) }

                ]
            }) 
            let signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
            const body = {
                data: data,
                signature: helpers.arrayToBase64(signature)
            }
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => helpers.handleFetchError(response))
            .catch( (e) => {
                console.error(e)
                return})

        }
        
        async getPublicKey(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)// for addblock getting public keys of other users
        {
            const url = "http://" + nodeIp + ":"+nodePort+"/user/"+emailId 
            var ePublicKey = await fetch(url, {
                method: "GET"
            })
            .then(async (response) => {helpers.handleFetchError(response)
                return await response.json()
                .then(
                    async (responseJson) => {
                        // console.log(await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey))))
                        return await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                        //return await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                    }
                )
            })
            .catch(e => {
                console.error("Error occered while fetching public keys :",e)
                return
            })
            return ePublicKey
            
        }

        async register(email,password,nodeIp=this.localNodeIP,nodePort=this.localNodePort,name,sex)//ePublicKey, sPublicKey , emailid, metadata[Optional]
        // No more password encryption of privatekey
        {   
            console.log(name)
            console.log(sex)
            //var iv="iv"
            //console.log ("User entered password: " + password) 
            var eKeyPair = await crypt.generate_RSA_Keypair() 
            var sKeyPair = await crypt.generate_RSASSA_KeyPair() 
            //console.log("Generated Signature key pair: \n")
            //console.log(sKeyPair)
            //console.log("Generated Encryption key pair: \n")
            //console.log(eKeyPair) 
            //https://stackoverflow.com/questions/957537/how-can-i-display-a-javascript-object
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            Window.ePrivateKey = eKeyPair.keyPairCrypto.privateKeyCrypto
            Window.sPrivateKey = sKeyPair.keyPairCrypto.privateKeyCrypto
            Window.ePublicKey = eKeyPair.keyPairCrypto.publicKeyCrypto
            Window.sPublicKey = sKeyPair.keyPairCrypto.publicKeyCrypto
                       
            await this.storePublicKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto, sKeyPair.keyPairCrypto.publicKeyCrypto,nodeIp,nodePort)
            await this.storeKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto,sKeyPair.keyPairCrypto.publicKeyCrypto, eKeyPair.keyPairCrypto.privateKeyCrypto, sKeyPair.keyPairCrypto.privateKeyCrypto,password,nodeIp,nodePort) 
            let userIdentityParams = {
                name: name,
                sex: sex,
                emailid:email
            }
            console.log(userIdentityParams)
            await this.login(email,password,nodeIp,nodePort)
            //create a file for user indentity management 
            let userIdentityFile = new File([JSON.stringify(userIdentityParams)],"userIdentityFile",{type: "text/plain"})
            //upload the file to ipfs
            var a = await this.uploadFile(userIdentityFile,email,email,true,nodeIp,nodePort,this.localIpfsIp,this.localIpfsPort)
            // console.log(a)
        
        
        }
        async storePublicKeys(emailId,ePublicKeyCrypto, sPublicKeyCrypto,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {//Backend registration
            const url = "http://" + nodeIp + ":" + nodePort + "/user" 
            //let encryptedSkey = await crypt.encryptkey(password,await crypt.getPkcs8(sPublicKeyCrypto))
            const data =
            JSON.stringify({
                emailId: emailId,
                ePublicKey: await crypt.getPem(ePublicKeyCrypto),
                sPublicKey: await crypt.getPem(sPublicKeyCrypto)
            } )
            let signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: helpers.arrayToBase64(signature)
            }
            await fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
            .then((response)=>{
                helpers.handleFetchError(response)
                response.json().then((respJson)=>(
                    alert(JSON.stringify(respJson.emailId, null, 2)))
                )
            })
            .catch( e => { 
                console.error('Error while storing public keys: '+e)
                return
            })
        }
        async storeKeys(emailId, ePublicKeyCrypto, sPublicKeyCrypto, ePrivateKeyCrypto, sPrivateKeyCrypto,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {
            //console.log(ePrivateKeyCrypto)
            //console.log(sPrivateKeyCrypto)
            //ecnrypt the privatekeys with user's password 
            const url = "http://" + nodeIp + ":" + nodePort + "/key" 
            let encryptedEKey = await crypt.encryptkey(password,await crypt.getPkcs8(ePrivateKeyCrypto))
            let encryptedSkey = await crypt.encryptkey(password,await crypt.getPkcs8(sPrivateKeyCrypto))
            const body =
            {

                emailId: emailId,
                sPublicKey: await crypt.getPem(sPublicKeyCrypto),
                ePublicKey: await crypt.getPem(ePublicKeyCrypto),
                sPrivateKey: helpers.arrayToBase64(encryptedSkey.encryptedKeyBuffer),//returns a string
                ePrivateKey: helpers.arrayToBase64(encryptedEKey.encryptedKeyBuffer),
                IV:{
                    ive: helpers.arrayToBase64(encryptedEKey.iv),
                    ivs: helpers.arrayToBase64(encryptedSkey.iv)
                }
            }
            await fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => helpers.handleFetchError(response))
            .catch( e=> {
                console.error('Error while stroing private keys :'+e)
                return})
        }
        async login(email,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            //create session variables for the user
            Window.mbUser = email
            await this.getPrivateKeys(email,password,nodeIp,nodePort)
            await this.getPublicKeys(email,nodeIp,nodePort)
        }
        async nuke(nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
	    logout();
 		const url = "http://" + nodeIp + ":"+ nodePort +"/nuke"
            fetch(url, {
                method: "POST"
            })
                .then((response) => {response.text().then((rj) => alert(rj))})
        }
        async getPublicKeys(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//fetch public key and set session variable
        {
            const url = "http://" + nodeIp + ":"+ nodePort +"/user/"+emailId 
            fetch(url, {
                method: "GET"
            })
                .then((response) => 
                {   helpers.handleFetchError(response)
                    response.json()
                    .then(async (responseJson) => {
                        Window.ePublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                        Window.sPublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.sPublicKey)))
                        console.log(Window.ePublicKey)
                        console.log(Window.sPublicKey)
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting public keys: '+e)
                    return
                })
        }
        async getPrivateKeys(emailId,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)
        {
            const url = "http://"+nodeIp +":"+nodePort+"/key/"+emailId 
            await fetch(url, {
                method: "GET"
            })
                .then(async (response) => {
                    helpers.handleFetchError(response)
                    await response.json()
                    .then(async (responseJson) => {
                        // console.log(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.ePrivateKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ive)))
                        Window.ePrivateKey = await crypt.getEPrivateKeyFromBuffer(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.ePrivateKey),helpers.base64ToArrayBuffer(responseJson.IV.IVE)))
                        Window.sPrivateKey = await crypt.getSPrivateKeyFromBuffer(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.sPrivateKey),helpers.base64ToArrayBuffer(responseJson.IV.IVS)))
                        // Window.ePublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                        // Window.sPublicKey = await crypt.getSPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.sPublicKey)))                        
                        console.log(Window.ePrivateKey)
                        console.log(Window.sPrivateKey)
                        // console.log(Window.sPublicKey)
                        // console.log(Window.ePublicKey)
                        //console.log(helpers.convertArrayBuffertoString(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.sPublicKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ivsp))))
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting private keys: '+e)
                    return
                })
        }
        async uploadFile(file, creatorEmailId, ownerEmailId, isIdentity, nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //1:Generate Random Aes Key and its params
            let aesKey = await crypt.generate_AES_Key()
            let iv = window.crypto.getRandomValues(new Uint8Array(16))
            //2: Get Hash
            const url = "http://" + ipfsNodeIp + ":"+ipfsPort+"/api/v0/block/put" 
            var reader = new FileReader()
            //fetch doesnt work with local files so I used reader 
            reader.addEventListener("load", async (result) => {
                var fileStream = new FormData() 
                //3: Encrypt the file with Aes Key
                var encryptedFile = await crypt.encryptFileAes(reader.result, aesKey, iv)//add iv
                fileStream.append('path', (new Blob([(encryptedFile)])))
                fetch(url, {
                    method: 'POST',
                    body: fileStream,
                })
                    .then(r => {
                        helpers.handleFetchError(r)
                        var hash = r.json()
                            .then(
                                async (response) => {
                                    console.log(response)
                                    //3: AddBlock 
                                    await this.addBlock(response.Key, file.name, file.type, creatorEmailId, ownerEmailId, aesKey, iv,nodeIp,nodePort)
                                    if (isIdentity==true){
                                        await this.updateIdentity(response.Key, ownerEmailId)
                                    }
                                }
                            )
                    }
                    )
            })
            reader.readAsArrayBuffer(file)
        }
        async uploadFiles(fileList, creatorEmailId, ownerEmailId,nodeIp = this.localNodeIP ,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //for loop for multiple files
            fileList.forEach(file => this.uploadFile(file, creatorEmailId, ownerEmailId, false,nodeIp,nodePort,ipfsNodeIp,ipfsPort))
        }
        async addBlock(hash, name, format, creatorEmailId, ownerEmailId, aesKey, iv, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            let ownerKeyBuffer = (await crypt.encryptAesKey(await crypt.exportAes(aesKey), await this.getPublicKey(ownerEmailId,nodeIp,nodePort)))
            console.log(ownerKeyBuffer)
            const url = "http://" + nodeIp + ":"+nodePort+"/block"
            const data = JSON.stringify([{
                ipfsHash: hash,
                name: name,
                format: format,
                signatoryEmailId: creatorEmailId,
                ownerEmailId: ownerEmailId,
                permissions: [
                    { receiverEmailId: ownerEmailId, receiverKey: helpers.arrayToBase64(ownerKeyBuffer) }

                ],
                IV:helpers.arrayToBase64(iv)
            }])
            // Generate Signature
            let signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: helpers.arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    helpers.handleFetchError(response)
                    return response.json()
                })

        }
        async updateIdentity(hash, ownerEmailId, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            const url = "http://" + nodeIp + ":"+nodePort+"/user/identity"
            const data = JSON.stringify({
                identityFileHash: hash,
                emailId: ownerEmailId
            })
            // Generate Signature
            let signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: helpers.arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    helpers.handleFetchError(response)
                    return response.json()
                })

        }

        async getBlock(hash,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//window object error
        {
            const url = "http://"+nodeIp+":"+nodePort+"/block/"+hash
            var block = await fetch(url, {
                method: "GET"
            })
            .then(async (response) => {
                helpers.handleFetchError(response)
                const responseJson = await response.json();
                return responseJson;
            })
            .catch( (e)=> {
                console.error("Error while fetching block from backend :"+e)
                return})
            return block
        }
        async downloadBlock (hash,emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) 
        {
            let blockData = await this.getBlock(hash,nodeIp,nodePort)
            //check permissions
            let receiverKeyS
            for (let index=0; index < blockData.permissions.length; index++)
            {
                if(blockData.permissions[index].receiverEmailId== emailId){
                    receiverKeyS= blockData.permissions[index].receiverKey
                    break
                }
                
            }
            try 
            {
            if (receiverKeyS == undefined){
                throw new PermissionError()
            }}
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            
            let receiverKeyB = helpers.base64ToArrayBuffer(receiverKeyS)
            //login before you call this method
            let aesKeyBuffer = await crypt.decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await crypt.importAes(aesKeyBuffer)
            //get file buffer and decrypt with aes key
            let iv = helpers.base64ToArrayBuffer(blockData.IV)
            const url = "http://"+ipfsNodeIp+":"+ipfsPort+"/api/v0/block/get?arg="+hash
            var file = await fetch( url, {
                method:"POST",
            })
            .then(async (response)=> 
            {//cant convert to json
                helpers.handleFetchError(response)
                return await response.arrayBuffer().then(
                    async (encryptedFileBuffer) => {
                        let file = new File([await crypt.decryptFileAes(encryptedFileBuffer,aesKey,iv)],blockData.name,{type: blockData.format})
                        this.save(file)
                        return file
                    }
                )
            })	
            .catch((e)=>
            {
                console.error("Error occured while downloading block: "+e)
                return
            })
            
        }
        
        save(file) {
            //var blob = new Blob([data], {type:type }) 
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(file, file.name) 
            }
            else{
                var elem = window.document.createElement('a') 
                elem.href = window.URL.createObjectURL(file) 
                elem.download = file.name         
                document.body.appendChild(elem) 
                elem.click()         
                document.body.removeChild(elem) 
            }
        }
        
        //Note that, depending on your situation, you may also want to call URL.revokeObjectURL after removing elem. 
        //According to the docs for URL.createObjectURL:
        
        //Each time you call createObjectURL(), a new object URL is created, 
        //even if you've already created one for the same object. Each of these must be released by calling URL.revokeObjectURL() 
        //when you no longer need them. Browsers will release these automatically when the document is unloaded  
        //however, for optimal performance and memory usage, if there are safe times when you can explicitly unload them, 
        //you should do so.

        async listBlock (nodeIp=this.localNodeIP,nodePort=this.localNodePort,ownerEmailId,permittedEmailId)
        {
            const url = "http://"+nodeIp+":"+nodePort+"/blocks"
            var body = {}
            if (ownerEmailId!=undefined && permittedEmailId!= undefined)
            {
                body.ownerEmailId= arguments[2]
                body.permittedEmailId= arguments[3]
            }
            if (permittedEmailId==undefined)
            {
                body.ownerEmailId= arguments[2]
            }
            if (ownerEmailId==undefined)
            {
                body.permittedEmailId= permittedEmailId
            }
            if (ownerEmailId==undefined && permittedEmailId==undefined)
            {
                body.ownerEmailId= ""
                body.permittedEmailId= ""
            }
            fetch(url, {
                method:"POST",
                body:JSON.stringify(body)
            })
            .then((response) => helpers.handleFetchError(response))
            .catch(e => {
                console.error(e)
                return})
        }
        async logout ()
        {
            //document.getElementById('current_user').innerHTML= ""
            delete Window.mbUser
            delete Window.ePublicKey
            delete Window.sPublicKey
            delete Window.ePrivateKey
            delete Window.sPrivateKey
        }
        //Asset functions

        async newAsset(assetName,assetAdmin=Window.mbUser,assetMinters,initBalance,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.newAsset(assetName,assetAdmin,assetMinters,initBalance,nodeIp,nodePort)
        }
        async getMinters (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort) {
            asset.getMinters(assetName,nodeIp,nodePort)
        }
        async changeAdmin(assetName,assetAdmin=this.mbUser,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.changeAdmin(assetName,assetAdmin,nodeIp,nodePort)
        }
        async burn (assetName,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.burn(assetName,amount,nodeIp,nodePort)
        }
        async totalSupply  (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.totalSupply(assetName,nodeIp,nodePort)
        }
        async transfer (assetName,from=this.mbUser,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.transfer(assetName,from,to,amount,nodeIp,nodePort)
        }
        async transferToAdmin  (assetName,from,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.transferToAdmin(assetName,from,amount,nodeIp,nodePort)
        }
        async transferFromAdmin  (assetName,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.transferFromAdmin(assetName,to,amount,nodeIp,nodePort)
        }
        async addMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.addMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async removeMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.removeMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async mint (assetName,target,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {	
		var minter=Window.mbUser
            asset.mint(assetName,minter,target,amount,nodeIp,nodePort)
        }
        async checkAdminBalance(assetName,nodeIp=this.localNodeIP,nodePort= this.localNodePort)
        {
            asset.checkAdminBalance(assetName,nodeIp,nodePort)
        }
        async checkBalance (assetName,target,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            asset.checkBalance(assetName,target,nodeIp,nodePort)
        }

    }
    window.Medblocks = Medblocks 
})() 
