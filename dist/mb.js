/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/medblocks.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/assets.js":
/*!**************************!*\
  !*** ./src/js/assets.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/* harmony import */ var _crypto_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./crypto.js */ "./src/js/crypto.js");
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");



var asset = {}
// assetAdmin is logged in user
asset.newAsset = async (assetName,assetAdmin,assetMinters,balance,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        assetAdmin: assetAdmin,
        assetMinters: assetMinters,
        balance: parseInt(balance)
        })
    // console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/newAsset"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while creating new asset: ' + e)
        })
}

asset.getMinters = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/getMinters"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting minters: ' + e)
        })
}

/*
data:{
                AssetName  string `json:"assetName"`
                NewAdmin   string `json:"newAdmin"`
},
signature (admin)
*/
asset.changeAdmin = async (assetName,assetAdmin,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        newAdmin: assetAdmin,
        })
    console.log(data)
    try {
        if(Window.ePrivateKey == undefined)
        throw new _errors_js__WEBPACK_IMPORTED_MODULE_2__["LoginError"]()
    }
    catch (e){
        console.log(e)
        return
    }
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/changeAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while changing asset admin: ' + e)
        })
}
/*
data:{
               AssetName    string   `json:"assetName"`
               Amount       int      `json:"amount"`
},
signature (minter)
*/
asset.burn = async (assetName,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/burn"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while burning: ' + e)
        })
}

asset.totalSupply  = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/totalSupply"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting total supply: ' + e)
        })
}
/*
data:{
                AssetName   string `json:"assetName"`
                From        string `json:"from"`
                To          string `json:"to"`
                Amount      int `json:"amount"`
},
signature (From)
*/

asset.transfer = async (assetName,from,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        to:to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transfer"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     From        string `json:"from"`
//     Amount      int `json:"amount"`
// },
// signature (From)

asset.transferToAdmin = async (assetName,from,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferToAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering to admin: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     To          string `json:"to"`
//     Amount      int `json:"amount"`
// },
// signature (admin)

asset.transferFromAdmin = async (assetName,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        to: to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferFromAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering from admin: ' + e)
        })
}



//data:{
//     AssetName    string   `json:"assetName"`
//     Minters      []string `json:"minters"`
// },
//signature (admin)
asset.addMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/addMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while adding minters: ' + e)
        })
}

asset.removeMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/removeMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while removing minters: ' + e)
        })
}

// // data:{
// //     AssetName    string   `json:"assetName"`
// //      Minter       string   `json:"minter"`
// //      Target       string   `json:"target"`
// //      Amount       int      `json:"amount"`
// // },
// // signature (minter)

asset.mint = async (assetName,minter,target,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minter: minter,
        target: target,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data))
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/mint"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while minting: ' + e)
        })
}


asset.checkBalance = (assetName,target,nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOf"      
    const body = {
        assetName: assetName,
        target: target
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
asset.checkAdminBalance = (assetName, nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOfAdmin"      
    const body = {
        assetName: assetName
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
/* harmony default export */ __webpack_exports__["default"] = (asset);


/***/ }),

/***/ "./src/js/crypto.js":
/*!**************************!*\
  !*** ./src/js/crypto.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/**
    * SECTION: crypto.js
    */

/*
* Contains all necessary cryptographic functions imlemented through webcrypto
*/

var crypt = {}
//const iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105]) 

crypt.generate_RSA_Keypair = async () => {
    let generatedKey = await crypto.subtle.generateKey
        (
            //algorithmIdentifier
            {
                "name": "RSA-OAEP",
                modulusLength: 2048,
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash:
                {
                    name: 'SHA-256'
                }

            },
            //boolean extractible true so exportKey can be called
            true,
            //keyUsages values set to encrypt,decrypt
            ["encrypt", "decrypt"]
        )
        .then(function (keypair) {
            return keypair
        })
        .catch((e) => { console.error("Error occured during RSA-OEAP key generation Message :" + e) })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_RSASSA_KeyPair = async () => {
    var generatedKey = await window.crypto.subtle.generateKey
        (
            {
                name: "RSASSA-PKCS1-v1_5",
                modulusLength: 2048, //can be 1024, 2048, or 4096
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
            },
            true, //whether the key is extractable (i.e. can be used in exportKey)
            ["sign", "verify"] //can be any combination of "sign" and "verify"
        )
        .then(function (keypair) {
            return keypair
        })
        .catch(function (e) {
            console.error(e)
        })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_AES_Key = async () => {
    var key = await window.crypto.subtle.generateKey(
        {
            name: "AES-GCM",
            length: 256, //can be  128, 192, or 256
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns a key object
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}

crypt.getPkcs8 = async (privateKeyCrypto) => {
    var pk_pkcs8 = await window.crypto.subtle.exportKey(//returns array buffer
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata //arrayBuffer of the private key
        })
        .catch(function (e) {
            console.error(e)
        })
    return pk_pkcs8
}
crypt.getPem = async (publicKeyCrypto) => {
    var spki = await window.crypto.subtle.exportKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    let pem = _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].spkiToPEM(spki)
    return pem
}
crypt.getPrivateKeyFromBuffer = async (priavteKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return privateKeyCrypto
}
crypt.getSPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var sPrivateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["sign"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return sPrivateKeyCrypto
}
crypt.getEPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error("error decrypting from decrypted buffer" + e)
        })
    return privateKeyCrypto
}
crypt.getEPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var publicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,//pass arraybuffer of the public key
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch((e) => {
            console.error(e, e.stack)
        })
    return publicKeyCrypto
}
crypt.getSPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var ePublicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["verify"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return ePublicKeyCrypto
}
crypt.encryptAesKey = async (aesKeyBuffer, publicKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var encryptedAesKey = await window.crypto.subtle.encrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        publicKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedAesKey//encrypt aes key with public key 
}
crypt.decryptAesKey = async (aesKeyBuffer, privateKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var decryptedAesKey = await window.crypto.subtle.decrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        privateKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to decrypt
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedAesKey//encrypt aes key with public key 
}
crypt.decrypt_AES = async (privateKeyString, password) => {
    var iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105])
    var key = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var enkey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        key, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    //step 2: use key to decrypt
    var decrypted_data = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        enkey, //from generateKey or importKey above
        //helpers.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        privateKeyString// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decrypted_data
}
crypt.encryptFileAes = async (fileBuffer, aesKey, iv) => {
    let encryptedFileBuffer = await window.crypto.subtle.encrypt(
        {
            name: "AES-GCM",

            //Don't re-use initialization vectors!
            //Always generate a new iv every time your encrypt!
            //Recommended to use 12 bytes length
            //iv: window.crypto.getRandomValues(new Uint8Array(12)),
            iv: iv,
            //Tag length (optional)
            tagLength: 128, //can be 32, 64, 96, 104, 112, 120 or 128 (default)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedFileBuffer
}
crypt.decryptFileAes = async (fileBuffer, aesKey, iv) => {
    let decryptedFileBuffer = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of the data
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedFileBuffer
}
crypt.getSignature = async (buffer) => {
    // if (Window.sPrivateKey== undefined){
    // 	this.getPrivateKey(emailId)
    // }
    let signature = await window.crypto.subtle.sign(
        {
            name: "RSASSA-PKCS1-v1_5",
        },
        Window.sPrivateKey, //from generateKey or importKey above
        buffer//ArrayBuffer of data you want to sign
    )
        .then(function (signature) {
            //returns an ArrayBuffer containing the signature
            return signature
        })
        .catch(function (e) {
            console.error(e)
        })
    return signature
}
crypt.exportAes = async (aesKey) => {
    let key = await window.crypto.subtle.exportKey(
        "raw", //can be "jwk" or "raw"
        aesKey //extractable must be true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.importAes = async (aesKeyBuffer) => {
    let key = await window.crypto.subtle.importKey(
        "raw", //can be "jwk" or "raw"
        aesKeyBuffer,
        {   //this is the algorithm options
            name: "AES-GCM",
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns the symmetric key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.encryptkey = async (password, keyBuffer) => {
    //create Initialization vector for aes key
    let iv = window.crypto.getRandomValues(new Uint8Array(16))
    //create a key from user's password
    let passwordKey = await window.crypto.subtle.importKey(
        "raw",
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
        { "name": "PBKDF2" },
        false,
        ["deriveKey"]
    )
    let encryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }
        },
        passwordKey,
        {
            "name": "AES-GCM",
            "length": 128
        },
        true,
        ["encrypt", "decrypt"]
    )
    let encryptedKeyData = await window.crypto.subtle.encrypt
        ({
            name: "AES-GCM",
            iv: iv
        },
            encryptionKey,
            keyBuffer
        )
    //let encryptedKeyBuffer = new Uint8Array(encryptedKeyData)
    return {
        encryptedKeyBuffer: encryptedKeyData,
        iv: iv
    }
}
crypt.decryptKey = async (password, keyBuffer, iv) => {
    //console.log(iv) 
    //console.log(keyBuffer)
    //console.log(password)
    //returns binary stream of private key
    var passwordKey = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var decryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        passwordKey, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        });
    //step 2: use key to decrypt
    var decryptedKeyData = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        decryptionKey, //from generateKey or importKey above
        //this.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        keyBuffer// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            //console.log("key decrypted: "+ decrypted)
            return decrypted
        })
        .catch(function (e) {
            console.error("Erroer while decrypting private key: " + e)
        })
    //console.log(decryptedKeyData)
    return decryptedKeyData
}

/* harmony default export */ __webpack_exports__["default"] = (crypt);

/***/ }),

/***/ "./src/js/errors.js":
/*!**************************!*\
  !*** ./src/js/errors.js ***!
  \**************************/
/*! exports provided: HttpError, PermissionError, LoginError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpError", function() { return HttpError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionError", function() { return PermissionError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginError", function() { return LoginError; });
class HttpError extends Error {
    constructor(response) {
        super()
        this.name = 'HttpError'
        this.message = 'Invalid Response status: ' + response.status
    }
}
class PermissionError extends Error {
    constructor() {
        super()
        this.name = 'PermissionError'
        this.message = 'Invalid Permissions for the requested medblock'
    }
}
class LoginError extends Error {
    constructor(){
    super()
    this.name = 'LoginError'
    this.message = 'Please make sure you are logged in to continue.'
    }
}


/***/ }),

/***/ "./src/js/helpers.js":
/*!***************************!*\
  !*** ./src/js/helpers.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");


var helpers = {}
helpers.base64ToArrayBuffer = (base64) => {
    var binary_string = window.atob(base64)
    var len = binary_string.length
    var bytes = new Uint8Array(len)
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i)
    }
    return bytes.buffer
}

helpers.arrayToBase64 = (array) => {
    return btoa(String.fromCharCode.apply(null, new Uint8Array(array)))
}
helpers.convertStringToArrayBuffer = (str) => { //needed for importkey I/P
    var encoder = new TextEncoder("utf-8")
    return encoder.encode(str)
}
helpers.convertArrayBuffertoString = (buffer) => {
    var decoder = new TextDecoder("utf-8")
    return decoder.decode(buffer)
}
// Calculates hexadecimal string representation of array buffer
helpers.hexString = (buffer) => {
    const byteArray = new Uint8Array(buffer)
    const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16)
        const paddedHexCode = hexCode.padStart(2, '0')
        return paddedHexCode
    })
    return hexCodes.join('')
}

helpers.removeLines = (pem) => {
    var lines = pem.split('\n')
    var encodedString = ''
    for (var i = 1; i < (lines.length - 1); i++) {
        encodedString += lines[i].trim()
    }
    return encodedString
}

helpers.spkiToPEM = (keyBuffer) => {
    // var keydataB64S = helpers.converArrayBuffertoString(keyBuffer) 
    // var keydatab64 = this.b64EncodeUnicode(keydataB64S)
    // var keydataB64Pem = this.formatAsPem(keydataB	64)//add new lines and split into 64
    var keydataB64 = helpers.arrayToBase64(keyBuffer)
    var keydataB64Pem = helpers.formatAsPem(keydataB64)
    return keydataB64Pem
}

helpers.formatAsPem = (str) => {
    var finalString = '-----BEGIN PUBLIC KEY-----\n'

    while (str.length > 0) {
        finalString += str.substring(0, 64) + '\n'
        str = str.substring(64)
    }

    finalString = finalString + "-----END PUBLIC KEY-----"

    return finalString
}
helpers.handleFetchError = (response) => {
    if (response.status !== 200 && response.status !== 202) {
        //console.error("Response Object: ")
        //console.error("Response object: "+JSON.stringify(response))
        throw new _errors_js__WEBPACK_IMPORTED_MODULE_0__["HttpError"](response)
    }
}
/* harmony default export */ __webpack_exports__["default"] = (helpers);

/***/ }),

/***/ "./src/js/medblocks.js":
/*!*****************************!*\
  !*** ./src/js/medblocks.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _assets_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets.js */ "./src/js/assets.js");
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/* harmony import */ var _crypto_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./crypto.js */ "./src/js/crypto.js");
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");




(function () {
    /**asset
     * SECTION: medblocks.js
     */
    
    /*
    * Main Medblocks class
    */

    class Medblocks {
        constructor(localNodeIP = "3.95.200.13",localNodePort = "8080",localIpfsIp = "3.95.200.13",localIpfsPort = "5001") {
            this.localNodeIP = localNodeIP, 
            this.localNodePort = localNodePort,
            this.localIpfsIp = localIpfsIp,
            this.localIpfsPort = localIpfsPort
        }
        async addPermission(hash,senderEmailId,receiverEmailId,nodeIp = this.localNodeIP,port = this.localNodePort)
        {
            
            //1: Get Block from blockchain and check if the loggedin user is the owner
            let block = await this.getBlock(hash,nodeIp,port)
            //2:Decrypt the receiverKey to get aes key
            let receiverKeyS
            for (let index=0 ; index < block.permissions.length ; index++)
            {
                if(block.permissions[index].receiverEmailId== senderEmailId){
                    receiverKeyS= block.permissions[index].receiverKey
                }
            }
            try
            {
            if (receiverKeyS==undefined)
            {   
                throw new _errors_js__WEBPACK_IMPORTED_MODULE_3__["PermissionError"]()    
            }
            }
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            let receiverKeyB = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(receiverKeyS)
            let aesKeyBuffer = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].importAes(aesKeyBuffer)
            //let receiverKey = block.permissions[0]["receiverKey"] works
            let receiverKeyBuffer = (await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptAesKey(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].exportAes(aesKey), await this.getPublicKey(receiverEmailId,nodeIp,port)))
            const url = "http://"+nodeIp+":"+port+"/block/permissions" 
            let data = JSON.stringify({
                ipfsHash: hash,
                senderEmailId: senderEmailId,
                permissions:  [
                    { receiverEmailId: receiverEmailId, receiverKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(receiverKeyBuffer) }

                ]
            }) 
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data)) 
            const body = {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch( (e) => {
                console.error(e)
                return})

        }
        
        async getPublicKey(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)// for addblock getting public keys of other users
        {
            const url = "http://" + nodeIp + ":"+nodePort+"/user/"+emailId 
            var ePublicKey = await fetch(url, {
                method: "GET"
            })
            .then(async (response) => {_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                return await response.json()
                .then(
                    async (responseJson) => {
                        // console.log(await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey))))
                        return await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.ePublicKey)))
                        //return await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                    }
                )
            })
            .catch(e => {
                console.error("Error occered while fetching public keys :",e)
                return
            })
            return ePublicKey
            
        }

        async register(email,password,nodeIp=this.localNodeIP,nodePort=this.localNodePort,name,sex)//ePublicKey, sPublicKey , emailid, metadata[Optional]
        // No more password encryption of privatekey
        {   
            console.log(name)
            console.log(sex)
            //var iv="iv"
            //console.log ("User entered password: " + password) 
            var eKeyPair = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_RSA_Keypair() 
            var sKeyPair = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_RSASSA_KeyPair() 
            //console.log("Generated Signature key pair: \n")
            //console.log(sKeyPair)
            //console.log("Generated Encryption key pair: \n")
            //console.log(eKeyPair) 
            //https://stackoverflow.com/questions/957537/how-can-i-display-a-javascript-object
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            Window.ePrivateKey = eKeyPair.keyPairCrypto.privateKeyCrypto
            Window.sPrivateKey = sKeyPair.keyPairCrypto.privateKeyCrypto
            Window.ePublicKey = eKeyPair.keyPairCrypto.publicKeyCrypto
            Window.sPublicKey = sKeyPair.keyPairCrypto.publicKeyCrypto
                       
            await this.storePublicKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto, sKeyPair.keyPairCrypto.publicKeyCrypto,nodeIp,nodePort)
            await this.storeKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto,sKeyPair.keyPairCrypto.publicKeyCrypto, eKeyPair.keyPairCrypto.privateKeyCrypto, sKeyPair.keyPairCrypto.privateKeyCrypto,password,nodeIp,nodePort) 
            let userIdentityParams = {
                name: name,
                sex: sex,
                emailid:email
            }
            console.log(userIdentityParams)
            await this.login(email,password,nodeIp,nodePort)
            //create a file for user indentity management 
            let userIdentityFile = new File([JSON.stringify(userIdentityParams)],"userIdentityFile",{type: "text/plain"})
            //upload the file to ipfs
            var a = await this.uploadFile(userIdentityFile,email,email,true,nodeIp,nodePort,this.localIpfsIp,this.localIpfsPort)
            // console.log(a)
        
        
        }
        async storePublicKeys(emailId,ePublicKeyCrypto, sPublicKeyCrypto,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {//Backend registration
            const url = "http://" + nodeIp + ":" + nodePort + "/user" 
            //let encryptedSkey = await crypt.encryptkey(password,await crypt.getPkcs8(sPublicKeyCrypto))
            const data =
            JSON.stringify({
                emailId: emailId,
                ePublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(ePublicKeyCrypto),
                sPublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(sPublicKeyCrypto)
            } )
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            await fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
            .then((response)=>{
                _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                response.json().then((respJson)=>(
                    alert(JSON.stringify(respJson.emailId, null, 2)))
                )
            })
            .catch( e => { 
                console.error('Error while storing public keys: '+e)
                return
            })
        }
        async storeKeys(emailId, ePublicKeyCrypto, sPublicKeyCrypto, ePrivateKeyCrypto, sPrivateKeyCrypto,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {
            //console.log(ePrivateKeyCrypto)
            //console.log(sPrivateKeyCrypto)
            //ecnrypt the privatekeys with user's password 
            const url = "http://" + nodeIp + ":" + nodePort + "/key" 
            let encryptedEKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptkey(password,await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPkcs8(ePrivateKeyCrypto))
            let encryptedSkey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptkey(password,await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPkcs8(sPrivateKeyCrypto))
            const body =
            {

                emailId: emailId,
                sPublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(sPublicKeyCrypto),
                ePublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(ePublicKeyCrypto),
                sPrivateKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedSkey.encryptedKeyBuffer),//returns a string
                ePrivateKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedEKey.encryptedKeyBuffer),
                IV:{
                    ive: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedEKey.iv),
                    ivs: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedSkey.iv)
                }
            }
            await fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch( e=> {
                console.error('Error while stroing private keys :'+e)
                return})
        }
        async login(email,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            //create session variables for the user
            Window.mbUser = email
            await this.getPrivateKeys(email,password,nodeIp,nodePort)
            await this.getPublicKeys(email,nodeIp,nodePort)
        }
        async nuke(nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
	    logout();
 		const url = "http://" + nodeIp + ":"+ nodePort +"/nuke"
            fetch(url, {
                method: "POST"
            })
                .then((response) => {response.text().then((rj) => alert(rj))})
        }
        async getPublicKeys(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//fetch public key and set session variable
        {
            const url = "http://" + nodeIp + ":"+ nodePort +"/user/"+emailId 
            fetch(url, {
                method: "GET"
            })
                .then((response) => 
                {   _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    response.json()
                    .then(async (responseJson) => {
                        Window.ePublicKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.ePublicKey)))
                        Window.sPublicKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.sPublicKey)))
                        console.log(Window.ePublicKey)
                        console.log(Window.sPublicKey)
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting public keys: '+e)
                    return
                })
        }
        async getPrivateKeys(emailId,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)
        {
            const url = "http://"+nodeIp +":"+nodePort+"/key/"+emailId 
            await fetch(url, {
                method: "GET"
            })
                .then(async (response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    await response.json()
                    .then(async (responseJson) => {
                        // console.log(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.ePrivateKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ive)))
                        Window.ePrivateKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPrivateKeyFromBuffer(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptKey(password,_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.ePrivateKey),_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.IV.IVE)))
                        Window.sPrivateKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSPrivateKeyFromBuffer(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptKey(password,_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.sPrivateKey),_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.IV.IVS)))
                        // Window.ePublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                        // Window.sPublicKey = await crypt.getSPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.sPublicKey)))                        
                        console.log(Window.ePrivateKey)
                        console.log(Window.sPrivateKey)
                        // console.log(Window.sPublicKey)
                        // console.log(Window.ePublicKey)
                        //console.log(helpers.convertArrayBuffertoString(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.sPublicKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ivsp))))
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting private keys: '+e)
                    return
                })
        }
        async uploadFile(file, creatorEmailId, ownerEmailId, isIdentity, nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //1:Generate Random Aes Key and its params
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_AES_Key()
            let iv = window.crypto.getRandomValues(new Uint8Array(16))
            //2: Get Hash
            const url = "http://" + ipfsNodeIp + ":"+ipfsPort+"/api/v0/block/put" 
            var reader = new FileReader()
            //fetch doesnt work with local files so I used reader 
            reader.addEventListener("load", async (result) => {
                var fileStream = new FormData() 
                //3: Encrypt the file with Aes Key
                var encryptedFile = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptFileAes(reader.result, aesKey, iv)//add iv
                fileStream.append('path', (new Blob([(encryptedFile)])))
                fetch(url, {
                    method: 'POST',
                    body: fileStream,
                })
                    .then(r => {
                        _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(r)
                        var hash = r.json()
                            .then(
                                async (response) => {
                                    console.log(response)
                                    //3: AddBlock 
                                    await this.addBlock(response.Key, file.name, file.type, creatorEmailId, ownerEmailId, aesKey, iv,nodeIp,nodePort)
                                    if (isIdentity==true){
                                        await this.updateIdentity(response.Key, ownerEmailId)
                                    }
                                }
                            )
                    }
                    )
            })
            reader.readAsArrayBuffer(file)
        }
        async uploadFiles(fileList, creatorEmailId, ownerEmailId,nodeIp = this.localNodeIP ,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //for loop for multiple files
            fileList.forEach(file => this.uploadFile(file, creatorEmailId, ownerEmailId, false,nodeIp,nodePort,ipfsNodeIp,ipfsPort))
        }
        async addBlock(hash, name, format, creatorEmailId, ownerEmailId, aesKey, iv, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            let ownerKeyBuffer = (await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptAesKey(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].exportAes(aesKey), await this.getPublicKey(ownerEmailId,nodeIp,nodePort)))
            console.log(ownerKeyBuffer)
            const url = "http://" + nodeIp + ":"+nodePort+"/block"
            const data = JSON.stringify([{
                ipfsHash: hash,
                name: name,
                format: format,
                signatoryEmailId: creatorEmailId,
                ownerEmailId: ownerEmailId,
                permissions: [
                    { receiverEmailId: ownerEmailId, receiverKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(ownerKeyBuffer) }

                ],
                IV:_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(iv)
            }])
            // Generate Signature
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    return response.json()
                })

        }
        async updateIdentity(hash, ownerEmailId, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            const url = "http://" + nodeIp + ":"+nodePort+"/user/identity"
            const data = JSON.stringify({
                identityFileHash: hash,
                emailId: ownerEmailId
            })
            // Generate Signature
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    return response.json()
                })

        }

        async getBlock(hash,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//window object error
        {
            const url = "http://"+nodeIp+":"+nodePort+"/block/"+hash
            var block = await fetch(url, {
                method: "GET"
            })
            .then(async (response) => {
                _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                const responseJson = await response.json();
                return responseJson;
            })
            .catch( (e)=> {
                console.error("Error while fetching block from backend :"+e)
                return})
            return block
        }
        async downloadBlock (hash,emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) 
        {
            let blockData = await this.getBlock(hash,nodeIp,nodePort)
            //check permissions
            let receiverKeyS
            for (let index=0; index < blockData.permissions.length; index++)
            {
                if(blockData.permissions[index].receiverEmailId== emailId){
                    receiverKeyS= blockData.permissions[index].receiverKey
                    break
                }
                
            }
            try 
            {
            if (receiverKeyS == undefined){
                throw new _errors_js__WEBPACK_IMPORTED_MODULE_3__["PermissionError"]()
            }}
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            
            let receiverKeyB = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(receiverKeyS)
            //login before you call this method
            let aesKeyBuffer = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].importAes(aesKeyBuffer)
            //get file buffer and decrypt with aes key
            let iv = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(blockData.IV)
            const url = "http://"+ipfsNodeIp+":"+ipfsPort+"/api/v0/block/get?arg="+hash
            var file = await fetch( url, {
                method:"POST",
            })
            .then(async (response)=> 
            {//cant convert to json
                _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                return await response.arrayBuffer().then(
                    async (encryptedFileBuffer) => {
                        let file = new File([await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptFileAes(encryptedFileBuffer,aesKey,iv)],blockData.name,{type: blockData.format})
                        this.save(file)
                        return file
                    }
                )
            })	
            .catch((e)=>
            {
                console.error("Error occured while downloading block: "+e)
                return
            })
            
        }
        
        save(file) {
            //var blob = new Blob([data], {type:type }) 
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(file, file.name) 
            }
            else{
                var elem = window.document.createElement('a') 
                elem.href = window.URL.createObjectURL(file) 
                elem.download = file.name         
                document.body.appendChild(elem) 
                elem.click()         
                document.body.removeChild(elem) 
            }
        }
        
        //Note that, depending on your situation, you may also want to call URL.revokeObjectURL after removing elem. 
        //According to the docs for URL.createObjectURL:
        
        //Each time you call createObjectURL(), a new object URL is created, 
        //even if you've already created one for the same object. Each of these must be released by calling URL.revokeObjectURL() 
        //when you no longer need them. Browsers will release these automatically when the document is unloaded  
        //however, for optimal performance and memory usage, if there are safe times when you can explicitly unload them, 
        //you should do so.

        async listBlock (nodeIp=this.localNodeIP,nodePort=this.localNodePort,ownerEmailId,permittedEmailId)
        {
            const url = "http://"+nodeIp+":"+nodePort+"/blocks"
            var body = {}
            if (ownerEmailId!=undefined && permittedEmailId!= undefined)
            {
                body.ownerEmailId= arguments[2]
                body.permittedEmailId= arguments[3]
            }
            if (permittedEmailId==undefined)
            {
                body.ownerEmailId= arguments[2]
            }
            if (ownerEmailId==undefined)
            {
                body.permittedEmailId= permittedEmailId
            }
            if (ownerEmailId==undefined && permittedEmailId==undefined)
            {
                body.ownerEmailId= ""
                body.permittedEmailId= ""
            }
            fetch(url, {
                method:"POST",
                body:JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch(e => {
                console.error(e)
                return})
        }
        async logout ()
        {
            //document.getElementById('current_user').innerHTML= ""
            delete Window.mbUser
            delete Window.ePublicKey
            delete Window.sPublicKey
            delete Window.ePrivateKey
            delete Window.sPrivateKey
        }
        //Asset functions

        async newAsset(assetName,assetAdmin=Window.mbUser,assetMinters,initBalance,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].newAsset(assetName,assetAdmin,assetMinters,initBalance,nodeIp,nodePort)
        }
        async getMinters (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort) {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].getMinters(assetName,nodeIp,nodePort)
        }
        async changeAdmin(assetName,assetAdmin=this.mbUser,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].changeAdmin(assetName,assetAdmin,nodeIp,nodePort)
        }
        async burn (assetName,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].burn(assetName,amount,nodeIp,nodePort)
        }
        async totalSupply  (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].totalSupply(assetName,nodeIp,nodePort)
        }
        async transfer (assetName,from=this.mbUser,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transfer(assetName,from,to,amount,nodeIp,nodePort)
        }
        async transferToAdmin  (assetName,from,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transferToAdmin(assetName,from,amount,nodeIp,nodePort)
        }
        async transferFromAdmin  (assetName,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transferFromAdmin(assetName,to,amount,nodeIp,nodePort)
        }
        async addMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].addMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async removeMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].removeMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async mint (assetName,target,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {	
		var minter=Window.mbUser
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].mint(assetName,minter,target,amount,nodeIp,nodePort)
        }
        async checkAdminBalance(assetName,nodeIp=this.localNodeIP,nodePort= this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].checkAdminBalance(assetName,nodeIp,nodePort)
        }
        async checkBalance (assetName,target,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].checkBalance(assetName,target,nodeIp,nodePort)
        }

    }
    window.Medblocks = Medblocks 
})() 


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2Fzc2V0cy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY3J5cHRvLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9lcnJvcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2hlbHBlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21lZGJsb2Nrcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQWtDO0FBQ0g7QUFDTztBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLHFEQUFVO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFFBQVEsbURBQU87QUFDZjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFFBQVEsbURBQU87QUFDZjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ2Usb0VBQUs7Ozs7Ozs7Ozs7Ozs7QUM5WnBCO0FBQUE7QUFBQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ2tDO0FBQ2xDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCx1QkFBdUIsOEVBQThFO0FBQ3JHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGtCQUFrQjtBQUN6QyxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVCxjQUFjLG1EQUFPO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckMsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQyxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQyxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckMsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLG1EQUFPO0FBQ3pCO0FBQ0EsbUJBQW1CLGdCQUFnQjtBQUNuQyxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsbURBQU87QUFDZixTQUFTLG1CQUFtQjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsbURBQU87QUFDekI7QUFDQSxtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLG1EQUFPO0FBQ3pCO0FBQ0EsbUJBQW1CLGdCQUFnQjtBQUNuQyxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRWUsb0U7Ozs7Ozs7Ozs7OztBQy9oQmY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQnFDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsU0FBUztBQUM1QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix3QkFBd0I7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixvREFBUztBQUMzQjtBQUNBO0FBQ2Usc0U7Ozs7Ozs7Ozs7OztBQ3hFZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWdDO0FBQ0U7QUFDSDtBQUN1QjtBQUN0RDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG1DQUFtQztBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYTtBQUNBLDBCQUEwQiwwREFBZTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixtREFBTztBQUN0QyxxQ0FBcUMsa0RBQUs7QUFDMUMsK0JBQStCLGtEQUFLO0FBQ3BDO0FBQ0EsMkNBQTJDLGtEQUFLLHFCQUFxQixrREFBSztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLGdEQUFnRCxtREFBTzs7QUFFNUU7QUFDQSxhQUFhO0FBQ2Isa0NBQWtDLGtEQUFLLGNBQWMsbURBQU87QUFDNUQ7QUFDQTtBQUNBLDJCQUEyQixtREFBTztBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixnQ0FBZ0MsbURBQU87QUFDdkM7QUFDQTtBQUNBLHVCQUF1Qjs7QUFFdkI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYix1Q0FBdUMsbURBQU87QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMsa0RBQUsseUJBQXlCLG1EQUFPLHFCQUFxQixtREFBTztBQUN0RztBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxTO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsa0RBQUs7QUFDdEMsaUNBQWlDLGtEQUFLO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxR0FBcUcsbUJBQW1CO0FBQ3hIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLGtEQUFLO0FBQ3ZDLGtDQUFrQyxrREFBSztBQUN2QyxhQUFhO0FBQ2Isa0NBQWtDLGtEQUFLLGNBQWMsbURBQU87QUFDNUQ7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLG1EQUFPO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsZ0JBQWdCLG1EQUFPO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYiwwQjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0Msa0RBQUssMkJBQTJCLGtEQUFLO0FBQzNFLHNDQUFzQyxrREFBSywyQkFBMkIsa0RBQUs7QUFDM0U7QUFDQTs7QUFFQTtBQUNBLGtDQUFrQyxrREFBSztBQUN2QyxrQ0FBa0Msa0RBQUs7QUFDdkMsNkJBQTZCLG1EQUFPO0FBQ3BDLDZCQUE2QixtREFBTztBQUNwQztBQUNBLHlCQUF5QixtREFBTztBQUNoQyx5QkFBeUIsbURBQU87QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixnQ0FBZ0MsbURBQU87QUFDdkM7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUNBQXFDLHdDQUF3QztBQUM3RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxpQkFBaUIsR0FBRyxtREFBTztBQUMzQjtBQUNBO0FBQ0Esa0RBQWtELGtEQUFLLHlCQUF5QixtREFBTyxxQkFBcUIsbURBQU87QUFDbkgsa0RBQWtELGtEQUFLLHlCQUF5QixtREFBTyxxQkFBcUIsbURBQU87QUFDbkg7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxvQkFBb0IsbURBQU87QUFDM0I7QUFDQTtBQUNBO0FBQ0EsbURBQW1ELGtEQUFLLGdDQUFnQyxrREFBSyxxQkFBcUIsbURBQU8sK0NBQStDLG1EQUFPO0FBQy9LLG1EQUFtRCxrREFBSyxnQ0FBZ0Msa0RBQUsscUJBQXFCLG1EQUFPLCtDQUErQyxtREFBTztBQUMvSztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSwrQkFBK0Isa0RBQUs7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQyxrREFBSztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLHdCQUF3QixtREFBTztBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esd0NBQXdDLGtEQUFLLHFCQUFxQixrREFBSztBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsNkNBQTZDLG1EQUFPOztBQUV6RTtBQUNBLG1CQUFtQixtREFBTztBQUMxQixhQUFhO0FBQ2I7QUFDQSxrQ0FBa0Msa0RBQUssY0FBYyxtREFBTztBQUM1RDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsbURBQU87QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxvQkFBb0IsbURBQU87QUFDM0I7QUFDQSxpQkFBaUI7O0FBRWpCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0Esa0NBQWtDLGtEQUFLLGNBQWMsbURBQU87QUFDNUQ7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLG1EQUFPO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0Esb0JBQW9CLG1EQUFPO0FBQzNCO0FBQ0EsaUJBQWlCOztBQUVqQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsZ0JBQWdCLG1EQUFPO0FBQ3ZCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixzQ0FBc0M7QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQiwwREFBZTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0JBQStCLG1EQUFPO0FBQ3RDO0FBQ0EscUNBQXFDLGtEQUFLO0FBQzFDLCtCQUErQixrREFBSztBQUNwQztBQUNBLHFCQUFxQixtREFBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCLG1EQUFPO0FBQ3ZCO0FBQ0E7QUFDQSxtREFBbUQsa0RBQUssZ0VBQWdFLHVCQUF1QjtBQUMvSTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7O0FBRWI7O0FBRUE7QUFDQSwyQ0FBMkMsV0FBVztBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLGdDQUFnQyxtREFBTztBQUN2QztBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQSxTO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQyIsImZpbGUiOiJtYi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2pzL21lZGJsb2Nrcy5qc1wiKTtcbiIsImltcG9ydCBoZWxwZXJzIGZyb20gJy4vaGVscGVycy5qcydcbmltcG9ydCBjcnlwdCBmcm9tICcuL2NyeXB0by5qcydcbmltcG9ydCB7TG9naW5FcnJvcn0gZnJvbSAnLi9lcnJvcnMuanMnXG52YXIgYXNzZXQgPSB7fVxuLy8gYXNzZXRBZG1pbiBpcyBsb2dnZWQgaW4gdXNlclxuYXNzZXQubmV3QXNzZXQgPSBhc3luYyAoYXNzZXROYW1lLGFzc2V0QWRtaW4sYXNzZXRNaW50ZXJzLGJhbGFuY2Usbm9kZUlwLG5vZGVQb3J0KSA9PiB7XG4gICAgY29uc3QgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXG4gICAgICAgIGFzc2V0QWRtaW46IGFzc2V0QWRtaW4sXG4gICAgICAgIGFzc2V0TWludGVyczogYXNzZXRNaW50ZXJzLFxuICAgICAgICBiYWxhbmNlOiBwYXJzZUludChiYWxhbmNlKVxuICAgICAgICB9KVxuICAgIC8vIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgIGRhdGE6IGRhdGEsXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcbiAgICB9XG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9uZXdBc3NldFwiICAgICAgICBcbiAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGNyZWF0aW5nIG5ldyBhc3NldDogJyArIGUpXG4gICAgICAgIH0pXG59XG5cbmFzc2V0LmdldE1pbnRlcnMgPSBhc3luYyAoYXNzZXROYW1lLG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lXG4gICAgICAgIH0pXG4gICAgY29uc29sZS5sb2coZGF0YSlcbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL2dldE1pbnRlcnNcIiAgICAgICAgXG4gICAgZmV0Y2godXJsLCB7XG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgIGJvZHk6IGRhdGFcbiAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGdldHRpbmcgbWludGVyczogJyArIGUpXG4gICAgICAgIH0pXG59XG5cbi8qXG5kYXRhOntcbiAgICAgICAgICAgICAgICBBc3NldE5hbWUgIHN0cmluZyBganNvbjpcImFzc2V0TmFtZVwiYFxuICAgICAgICAgICAgICAgIE5ld0FkbWluICAgc3RyaW5nIGBqc29uOlwibmV3QWRtaW5cImBcbn0sXG5zaWduYXR1cmUgKGFkbWluKVxuKi9cbmFzc2V0LmNoYW5nZUFkbWluID0gYXN5bmMgKGFzc2V0TmFtZSxhc3NldEFkbWluLG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBuZXdBZG1pbjogYXNzZXRBZG1pbixcbiAgICAgICAgfSlcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgIHRyeSB7XG4gICAgICAgIGlmKFdpbmRvdy5lUHJpdmF0ZUtleSA9PSB1bmRlZmluZWQpXG4gICAgICAgIHRocm93IG5ldyBMb2dpbkVycm9yKClcbiAgICB9XG4gICAgY2F0Y2ggKGUpe1xuICAgICAgICBjb25zb2xlLmxvZyhlKVxuICAgICAgICByZXR1cm5cbiAgICB9XG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgIGRhdGE6IGRhdGEsXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcbiAgICB9XG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9jaGFuZ2VBZG1pblwiICAgICAgICBcbiAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGNoYW5naW5nIGFzc2V0IGFkbWluOiAnICsgZSlcbiAgICAgICAgfSlcbn1cbi8qXG5kYXRhOntcbiAgICAgICAgICAgICAgIEFzc2V0TmFtZSAgICBzdHJpbmcgICBganNvbjpcImFzc2V0TmFtZVwiYFxuICAgICAgICAgICAgICAgQW1vdW50ICAgICAgIGludCAgICAgIGBqc29uOlwiYW1vdW50XCJgXG59LFxuc2lnbmF0dXJlIChtaW50ZXIpXG4qL1xuYXNzZXQuYnVybiA9IGFzeW5jIChhc3NldE5hbWUsYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBhbW91bnQ6IHBhcnNlSW50KGFtb3VudClcbiAgICAgICAgfSlcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgIGNvbnN0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcbiAgICBjb25zdCBib2R5ID0ge1xuICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgfVxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvYnVyblwiICAgICAgICBcbiAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGJ1cm5pbmc6ICcgKyBlKVxuICAgICAgICB9KVxufVxuXG5hc3NldC50b3RhbFN1cHBseSAgPSBhc3luYyAoYXNzZXROYW1lLG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lXG4gICAgICAgIH0pXG4gICAgY29uc29sZS5sb2coZGF0YSlcbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL3RvdGFsU3VwcGx5XCIgICAgICAgIFxuICAgIGZldGNoKHVybCwge1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBib2R5OiBkYXRhXG4gICAgfSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICAgIClcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSBnZXR0aW5nIHRvdGFsIHN1cHBseTogJyArIGUpXG4gICAgICAgIH0pXG59XG4vKlxuZGF0YTp7XG4gICAgICAgICAgICAgICAgQXNzZXROYW1lICAgc3RyaW5nIGBqc29uOlwiYXNzZXROYW1lXCJgXG4gICAgICAgICAgICAgICAgRnJvbSAgICAgICAgc3RyaW5nIGBqc29uOlwiZnJvbVwiYFxuICAgICAgICAgICAgICAgIFRvICAgICAgICAgIHN0cmluZyBganNvbjpcInRvXCJgXG4gICAgICAgICAgICAgICAgQW1vdW50ICAgICAgaW50IGBqc29uOlwiYW1vdW50XCJgXG59LFxuc2lnbmF0dXJlIChGcm9tKVxuKi9cblxuYXNzZXQudHJhbnNmZXIgPSBhc3luYyAoYXNzZXROYW1lLGZyb20sdG8sYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBmcm9tOiBmcm9tLFxuICAgICAgICB0bzp0byxcbiAgICAgICAgYW1vdW50OiBwYXJzZUludChhbW91bnQpXG4gICAgICAgIH0pXG4gICAgY29uc29sZS5sb2coZGF0YSlcbiAgICBjb25zdCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSkgXG4gICAgY29uc3QgYm9keSA9IHtcbiAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxuICAgIH1cbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL3RyYW5zZmVyXCIgICAgICAgIFxuICAgIGZldGNoKHVybCwge1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxuICAgIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICApXG4gICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgdHJhbnNmZXJpbmc6ICcgKyBlKVxuICAgICAgICB9KVxufVxuXG4vLyBkYXRhOntcbi8vICAgICBBc3NldE5hbWUgICBzdHJpbmcgYGpzb246XCJhc3NldE5hbWVcImBcbi8vICAgICBGcm9tICAgICAgICBzdHJpbmcgYGpzb246XCJmcm9tXCJgXG4vLyAgICAgQW1vdW50ICAgICAgaW50IGBqc29uOlwiYW1vdW50XCJgXG4vLyB9LFxuLy8gc2lnbmF0dXJlIChGcm9tKVxuXG5hc3NldC50cmFuc2ZlclRvQWRtaW4gPSBhc3luYyAoYXNzZXROYW1lLGZyb20sYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBmcm9tOiBmcm9tLFxuICAgICAgICBhbW91bnQ6IHBhcnNlSW50KGFtb3VudClcbiAgICAgICAgfSlcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgIGNvbnN0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcbiAgICBjb25zdCBib2R5ID0ge1xuICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgfVxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvdHJhbnNmZXJUb0FkbWluXCIgICAgICAgIFxuICAgIGZldGNoKHVybCwge1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxuICAgIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICApXG4gICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgdHJhbnNmZXJpbmcgdG8gYWRtaW46ICcgKyBlKVxuICAgICAgICB9KVxufVxuXG4vLyBkYXRhOntcbi8vICAgICBBc3NldE5hbWUgICBzdHJpbmcgYGpzb246XCJhc3NldE5hbWVcImBcbi8vICAgICBUbyAgICAgICAgICBzdHJpbmcgYGpzb246XCJ0b1wiYFxuLy8gICAgIEFtb3VudCAgICAgIGludCBganNvbjpcImFtb3VudFwiYFxuLy8gfSxcbi8vIHNpZ25hdHVyZSAoYWRtaW4pXG5cbmFzc2V0LnRyYW5zZmVyRnJvbUFkbWluID0gYXN5bmMgKGFzc2V0TmFtZSx0byxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KSA9PiB7XG4gICAgY29uc3QgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXG4gICAgICAgIHRvOiB0byxcbiAgICAgICAgYW1vdW50OiBwYXJzZUludChhbW91bnQpXG4gICAgICAgIH0pXG4gICAgY29uc29sZS5sb2coZGF0YSlcbiAgICBjb25zdCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSkgXG4gICAgY29uc3QgYm9keSA9IHtcbiAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxuICAgIH1cbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL3RyYW5zZmVyRnJvbUFkbWluXCIgICAgICAgIFxuICAgIGZldGNoKHVybCwge1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxuICAgIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICApXG4gICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgdHJhbnNmZXJpbmcgZnJvbSBhZG1pbjogJyArIGUpXG4gICAgICAgIH0pXG59XG5cblxuXG4vL2RhdGE6e1xuLy8gICAgIEFzc2V0TmFtZSAgICBzdHJpbmcgICBganNvbjpcImFzc2V0TmFtZVwiYFxuLy8gICAgIE1pbnRlcnMgICAgICBbXXN0cmluZyBganNvbjpcIm1pbnRlcnNcImBcbi8vIH0sXG4vL3NpZ25hdHVyZSAoYWRtaW4pXG5hc3NldC5hZGRNaW50ZXJzID0gYXN5bmMgKGFzc2V0TmFtZSxtaW50ZXJzLG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBtaW50ZXJzOiBtaW50ZXJzLFxuICAgICAgICB9KVxuICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxuICAgIGNvbnNvbGUubG9nKHNpZ25hdHVyZSlcbiAgICBjb25zdCBib2R5ID0ge1xuICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgfVxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvYWRkTWludGVyc1wiICAgICAgICBcbiAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgKVxuICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGFkZGluZyBtaW50ZXJzOiAnICsgZSlcbiAgICAgICAgfSlcbn1cblxuYXNzZXQucmVtb3ZlTWludGVycyA9IGFzeW5jIChhc3NldE5hbWUsbWludGVycyxub2RlSXAsbm9kZVBvcnQpID0+IHtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICBhc3NldE5hbWU6IGFzc2V0TmFtZSxcbiAgICAgICAgbWludGVyczogbWludGVycyxcbiAgICAgICAgfSlcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgIGNvbnN0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcbiAgICBjb25zb2xlLmxvZyhzaWduYXR1cmUpXG4gICAgY29uc3QgYm9keSA9IHtcbiAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxuICAgIH1cbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL3JlbW92ZU1pbnRlcnNcIiAgICAgICAgXG4gICAgZmV0Y2godXJsLCB7XG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXG4gICAgfSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICAgIClcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSByZW1vdmluZyBtaW50ZXJzOiAnICsgZSlcbiAgICAgICAgfSlcbn1cblxuLy8gLy8gZGF0YTp7XG4vLyAvLyAgICAgQXNzZXROYW1lICAgIHN0cmluZyAgIGBqc29uOlwiYXNzZXROYW1lXCJgXG4vLyAvLyAgICAgIE1pbnRlciAgICAgICBzdHJpbmcgICBganNvbjpcIm1pbnRlclwiYFxuLy8gLy8gICAgICBUYXJnZXQgICAgICAgc3RyaW5nICAgYGpzb246XCJ0YXJnZXRcImBcbi8vIC8vICAgICAgQW1vdW50ICAgICAgIGludCAgICAgIGBqc29uOlwiYW1vdW50XCJgXG4vLyAvLyB9LFxuLy8gLy8gc2lnbmF0dXJlIChtaW50ZXIpXG5cbmFzc2V0Lm1pbnQgPSBhc3luYyAoYXNzZXROYW1lLG1pbnRlcix0YXJnZXQsYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICBtaW50ZXI6IG1pbnRlcixcbiAgICAgICAgdGFyZ2V0OiB0YXJnZXQsXG4gICAgICAgIGFtb3VudDogcGFyc2VJbnQoYW1vdW50KVxuICAgICAgICB9KVxuICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpXG4gICAgY29uc29sZS5sb2coc2lnbmF0dXJlKVxuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgIGRhdGE6IGRhdGEsXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcbiAgICB9XG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9taW50XCIgICAgICAgIFxuICAgIGZldGNoKHVybCwge1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxuICAgIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICApXG4gICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgbWludGluZzogJyArIGUpXG4gICAgICAgIH0pXG59XG5cblxuYXNzZXQuY2hlY2tCYWxhbmNlID0gKGFzc2V0TmFtZSx0YXJnZXQsbm9kZUlwLG5vZGVQb3J0KSA9PiB7XG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9iYWxhbmNlT2ZcIiAgICAgIFxuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxuICAgICAgICB0YXJnZXQ6IHRhcmdldFxuICAgIH1cbiAgICBmZXRjaCAodXJsLCB7XG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXG4gICAgfSlcbiAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxuICAgICAgICByZXNwb25zZS5qc29uKClcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlSnNvbj0+IGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbikpKVxuICAgIH0pXG59XG5hc3NldC5jaGVja0FkbWluQmFsYW5jZSA9IChhc3NldE5hbWUsIG5vZGVJcCxub2RlUG9ydCkgPT4ge1xuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvYmFsYW5jZU9mQWRtaW5cIiAgICAgIFxuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lXG4gICAgfVxuICAgIGZldGNoICh1cmwsIHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICB9KVxuICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAudGhlbigocmVzcG9uc2VKc29uPT4gY29uc29sZS5sb2cocmVzcG9uc2VKc29uKSkpXG4gICAgfSlcbn1cbmV4cG9ydCBkZWZhdWx0IGFzc2V0XG4iLCIvKipcbiAgICAqIFNFQ1RJT046IGNyeXB0by5qc1xuICAgICovXG5cbi8qXG4qIENvbnRhaW5zIGFsbCBuZWNlc3NhcnkgY3J5cHRvZ3JhcGhpYyBmdW5jdGlvbnMgaW1sZW1lbnRlZCB0aHJvdWdoIHdlYmNyeXB0b1xuKi9cbmltcG9ydCBoZWxwZXJzIGZyb20gJy4vaGVscGVycy5qcydcbnZhciBjcnlwdCA9IHt9XG4vL2NvbnN0IGl2ID0gbmV3IFVpbnQ4QXJyYXkoWzE1NCwgMTg4LCA5MywgNjUsIDg3LCAxMzgsIDM3LCAyNTQsIDI0MCwgNzgsIDEwLCAxMDVdKSBcblxuY3J5cHQuZ2VuZXJhdGVfUlNBX0tleXBhaXIgPSBhc3luYyAoKSA9PiB7XG4gICAgbGV0IGdlbmVyYXRlZEtleSA9IGF3YWl0IGNyeXB0by5zdWJ0bGUuZ2VuZXJhdGVLZXlcbiAgICAgICAgKFxuICAgICAgICAgICAgLy9hbGdvcml0aG1JZGVudGlmaWVyXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiUlNBLU9BRVBcIixcbiAgICAgICAgICAgICAgICBtb2R1bHVzTGVuZ3RoOiAyMDQ4LFxuICAgICAgICAgICAgICAgIHB1YmxpY0V4cG9uZW50OiBuZXcgVWludDhBcnJheShbMHgwMSwgMHgwMCwgMHgwMV0pLFxuICAgICAgICAgICAgICAgIGhhc2g6XG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnU0hBLTI1NidcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAvL2Jvb2xlYW4gZXh0cmFjdGlibGUgdHJ1ZSBzbyBleHBvcnRLZXkgY2FuIGJlIGNhbGxlZFxuICAgICAgICAgICAgdHJ1ZSxcbiAgICAgICAgICAgIC8va2V5VXNhZ2VzIHZhbHVlcyBzZXQgdG8gZW5jcnlwdCxkZWNyeXB0XG4gICAgICAgICAgICBbXCJlbmNyeXB0XCIsIFwiZGVjcnlwdFwiXVxuICAgICAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXlwYWlyKSB7XG4gICAgICAgICAgICByZXR1cm4ga2V5cGFpclxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGUpID0+IHsgY29uc29sZS5lcnJvcihcIkVycm9yIG9jY3VyZWQgZHVyaW5nIFJTQS1PRUFQIGtleSBnZW5lcmF0aW9uIE1lc3NhZ2UgOlwiICsgZSkgfSlcbiAgICB2YXIganNvblByaXZhdGVLZXkgPSBhd2FpdCBjcnlwdG8uc3VidGxlLmV4cG9ydEtleShcImp3a1wiLCBnZW5lcmF0ZWRLZXkucHJpdmF0ZUtleSlcbiAgICB2YXIganNvblB1YmxpY0tleSA9IGF3YWl0IGNyeXB0by5zdWJ0bGUuZXhwb3J0S2V5KFwiandrXCIsIGdlbmVyYXRlZEtleS5wdWJsaWNLZXkpXG4gICAgcmV0dXJuIHtcbiAgICAgICAga2V5UGFpckpzb246XG4gICAgICAgIHtcbiAgICAgICAgICAgIHB1YmxpY0tleUpzb246IGpzb25QdWJsaWNLZXksLy9Kc29uIHdlYiBkaWN0aW9uYXJ5IG9iamVjdFxuICAgICAgICAgICAgcHJpdmF0ZUtleUpzb246IGpzb25Qcml2YXRlS2V5XG4gICAgICAgIH0sXG4gICAgICAgIGtleVBhaXJDcnlwdG86XG4gICAgICAgIHtcbiAgICAgICAgICAgIHB1YmxpY0tleUNyeXB0bzogZ2VuZXJhdGVkS2V5LnB1YmxpY0tleSwvL0NyeXB0b0tleSBvYmplY3RcbiAgICAgICAgICAgIHByaXZhdGVLZXlDcnlwdG86IGdlbmVyYXRlZEtleS5wcml2YXRlS2V5XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmNyeXB0LmdlbmVyYXRlX1JTQVNTQV9LZXlQYWlyID0gYXN5bmMgKCkgPT4ge1xuICAgIHZhciBnZW5lcmF0ZWRLZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5nZW5lcmF0ZUtleVxuICAgICAgICAoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJSU0FTU0EtUEtDUzEtdjFfNVwiLFxuICAgICAgICAgICAgICAgIG1vZHVsdXNMZW5ndGg6IDIwNDgsIC8vY2FuIGJlIDEwMjQsIDIwNDgsIG9yIDQwOTZcbiAgICAgICAgICAgICAgICBwdWJsaWNFeHBvbmVudDogbmV3IFVpbnQ4QXJyYXkoWzB4MDEsIDB4MDAsIDB4MDFdKSxcbiAgICAgICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTI1NlwiIH0sIC8vY2FuIGJlIFwiU0hBLTFcIiwgXCJTSEEtMjU2XCIsIFwiU0hBLTM4NFwiLCBvciBcIlNIQS01MTJcIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRydWUsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgICAgIFtcInNpZ25cIiwgXCJ2ZXJpZnlcIl0gLy9jYW4gYmUgYW55IGNvbWJpbmF0aW9uIG9mIFwic2lnblwiIGFuZCBcInZlcmlmeVwiXG4gICAgICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleXBhaXIpIHtcbiAgICAgICAgICAgIHJldHVybiBrZXlwYWlyXG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHZhciBqc29uUHJpdmF0ZUtleSA9IGF3YWl0IGNyeXB0by5zdWJ0bGUuZXhwb3J0S2V5KFwiandrXCIsIGdlbmVyYXRlZEtleS5wcml2YXRlS2V5KVxuICAgIHZhciBqc29uUHVibGljS2V5ID0gYXdhaXQgY3J5cHRvLnN1YnRsZS5leHBvcnRLZXkoXCJqd2tcIiwgZ2VuZXJhdGVkS2V5LnB1YmxpY0tleSlcbiAgICByZXR1cm4ge1xuICAgICAgICBrZXlQYWlySnNvbjpcbiAgICAgICAge1xuICAgICAgICAgICAgcHVibGljS2V5SnNvbjoganNvblB1YmxpY0tleSwvL0pzb24gd2ViIGRpY3Rpb25hcnkgb2JqZWN0XG4gICAgICAgICAgICBwcml2YXRlS2V5SnNvbjoganNvblByaXZhdGVLZXlcbiAgICAgICAgfSxcbiAgICAgICAga2V5UGFpckNyeXB0bzpcbiAgICAgICAge1xuICAgICAgICAgICAgcHVibGljS2V5Q3J5cHRvOiBnZW5lcmF0ZWRLZXkucHVibGljS2V5LC8vQ3J5cHRvS2V5IG9iamVjdFxuICAgICAgICAgICAgcHJpdmF0ZUtleUNyeXB0bzogZ2VuZXJhdGVkS2V5LnByaXZhdGVLZXlcbiAgICAgICAgfVxuICAgIH1cbn1cblxuY3J5cHQuZ2VuZXJhdGVfQUVTX0tleSA9IGFzeW5jICgpID0+IHtcbiAgICB2YXIga2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZ2VuZXJhdGVLZXkoXG4gICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiQUVTLUdDTVwiLFxuICAgICAgICAgICAgbGVuZ3RoOiAyNTYsIC8vY2FuIGJlICAxMjgsIDE5Miwgb3IgMjU2XG4gICAgICAgIH0sXG4gICAgICAgIHRydWUsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgW1wiZW5jcnlwdFwiLCBcImRlY3J5cHRcIl0gLy9jYW4gXCJlbmNyeXB0XCIsIFwiZGVjcnlwdFwiLCBcIndyYXBLZXlcIiwgb3IgXCJ1bndyYXBLZXlcIlxuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgLy9yZXR1cm5zIGEga2V5IG9iamVjdFxuICAgICAgICAgICAgcmV0dXJuIGtleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4ga2V5XG59XG5cbmNyeXB0LmdldFBrY3M4ID0gYXN5bmMgKHByaXZhdGVLZXlDcnlwdG8pID0+IHtcbiAgICB2YXIgcGtfcGtjczggPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5leHBvcnRLZXkoLy9yZXR1cm5zIGFycmF5IGJ1ZmZlclxuICAgICAgICBcInBrY3M4XCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXG4gICAgICAgIHByaXZhdGVLZXlDcnlwdG8gLy9jYW4gYmUgYSBwdWJsaWNLZXkgb3IgcHJpdmF0ZUtleSwgYXMgbG9uZyBhcyBleHRyYWN0YWJsZSB3YXMgdHJ1ZVxuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleWRhdGEpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyB0aGUgZXhwb3J0ZWQga2V5IGRhdGFcbiAgICAgICAgICAgIHJldHVybiBrZXlkYXRhIC8vYXJyYXlCdWZmZXIgb2YgdGhlIHByaXZhdGUga2V5XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHJldHVybiBwa19wa2NzOFxufVxuY3J5cHQuZ2V0UGVtID0gYXN5bmMgKHB1YmxpY0tleUNyeXB0bykgPT4ge1xuICAgIHZhciBzcGtpID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZXhwb3J0S2V5KFxuICAgICAgICBcInNwa2lcIiwgLy9jYW4gYmUgXCJqd2tcIiAocHVibGljIG9yIHByaXZhdGUpLCBcInNwa2lcIiAocHVibGljIG9ubHkpLCBvciBcInBrY3M4XCIgKHByaXZhdGUgb25seSlcbiAgICAgICAgcHVibGljS2V5Q3J5cHRvIC8vY2FuIGJlIGEgcHVibGljS2V5IG9yIHByaXZhdGVLZXksIGFzIGxvbmcgYXMgZXh0cmFjdGFibGUgd2FzIHRydWVcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXlkYXRhKSB7XG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGV4cG9ydGVkIGtleSBkYXRhXG4gICAgICAgICAgICByZXR1cm4ga2V5ZGF0YVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICBsZXQgcGVtID0gaGVscGVycy5zcGtpVG9QRU0oc3BraSlcbiAgICByZXR1cm4gcGVtXG59XG5jcnlwdC5nZXRQcml2YXRlS2V5RnJvbUJ1ZmZlciA9IGFzeW5jIChwcmlhdnRlS2V5QnVmZmVyKSA9PiB7XG4gICAgdmFyIHByaXZhdGVLZXlDcnlwdG8gPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXG4gICAgICAgIFwicGtjczhcIiwgLy9jYW4gYmUgXCJqd2tcIiAocHVibGljIG9yIHByaXZhdGUpLCBcInNwa2lcIiAocHVibGljIG9ubHkpLCBvciBcInBrY3M4XCIgKHByaXZhdGUgb25seSlcbiAgICAgICAgcHJpdmF0ZUtleUJ1ZmZlcixcbiAgICAgICAgeyAgIC8vdGhlc2UgYXJlIHRoZSBhbGdvcml0aG0gb3B0aW9uc1xuICAgICAgICAgICAgbmFtZTogXCJSU0EtT0FFUFwiLFxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcbiAgICAgICAgfSxcbiAgICAgICAgZmFsc2UsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgW1wiZGVjcnlwdFwiXSAvL1wiZW5jcnlwdFwiIG9yIFwid3JhcEtleVwiIGZvciBwdWJsaWMga2V5IGltcG9ydCBvclxuICAgICAgICAvL1wiZGVjcnlwdFwiIG9yIFwidW53cmFwS2V5XCIgZm9yIHByaXZhdGUga2V5IGltcG9ydHNcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhIHB1YmxpY0tleSAob3IgcHJpdmF0ZUtleSBpZiB5b3UgYXJlIGltcG9ydGluZyBhIHByaXZhdGUga2V5KVxuICAgICAgICAgICAgcmV0dXJuIGtleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4gcHJpdmF0ZUtleUNyeXB0b1xufVxuY3J5cHQuZ2V0U1ByaXZhdGVLZXlGcm9tQnVmZmVyID0gYXN5bmMgKHByaXZhdGVLZXlCdWZmZXIpID0+IHtcbiAgICB2YXIgc1ByaXZhdGVLZXlDcnlwdG8gPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXG4gICAgICAgIFwicGtjczhcIiwgLy9jYW4gYmUgXCJqd2tcIiAocHVibGljIG9yIHByaXZhdGUpLCBcInNwa2lcIiAocHVibGljIG9ubHkpLCBvciBcInBrY3M4XCIgKHByaXZhdGUgb25seSlcbiAgICAgICAgcHJpdmF0ZUtleUJ1ZmZlcixcbiAgICAgICAgeyAgIC8vdGhlc2UgYXJlIHRoZSBhbGdvcml0aG0gb3B0aW9uc1xuICAgICAgICAgICAgbmFtZTogXCJSU0FTU0EtUEtDUzEtdjFfNVwiLFxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcbiAgICAgICAgfSxcbiAgICAgICAgZmFsc2UsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgW1wic2lnblwiXSAvL1widmVyaWZ5XCIgZm9yIHB1YmxpYyBrZXkgaW1wb3J0LCBcInNpZ25cIiBmb3IgcHJpdmF0ZSBrZXkgaW1wb3J0c1xuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHB1YmxpY0tleSkge1xuICAgICAgICAgICAgLy9yZXR1cm5zIGEgcHVibGljS2V5IChvciBwcml2YXRlS2V5IGlmIHlvdSBhcmUgaW1wb3J0aW5nIGEgcHJpdmF0ZSBrZXkpXG4gICAgICAgICAgICByZXR1cm4gcHVibGljS2V5XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHJldHVybiBzUHJpdmF0ZUtleUNyeXB0b1xufVxuY3J5cHQuZ2V0RVByaXZhdGVLZXlGcm9tQnVmZmVyID0gYXN5bmMgKHByaXZhdGVLZXlCdWZmZXIpID0+IHtcbiAgICB2YXIgcHJpdmF0ZUtleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcbiAgICAgICAgXCJwa2NzOFwiLCAvL2NhbiBiZSBcImp3a1wiIChwdWJsaWMgb3IgcHJpdmF0ZSksIFwic3BraVwiIChwdWJsaWMgb25seSksIG9yIFwicGtjczhcIiAocHJpdmF0ZSBvbmx5KVxuICAgICAgICBwcml2YXRlS2V5QnVmZmVyLFxuICAgICAgICB7ICAgLy90aGVzZSBhcmUgdGhlIGFsZ29yaXRobSBvcHRpb25zXG4gICAgICAgICAgICBuYW1lOiBcIlJTQS1PQUVQXCIsXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTI1NlwiIH0sIC8vY2FuIGJlIFwiU0hBLTFcIiwgXCJTSEEtMjU2XCIsIFwiU0hBLTM4NFwiLCBvciBcIlNIQS01MTJcIlxuICAgICAgICB9LFxuICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXG4gICAgICAgIFtcImRlY3J5cHRcIl0gLy9cImVuY3J5cHRcIiBvciBcIndyYXBLZXlcIiBmb3IgcHVibGljIGtleSBpbXBvcnQgb3JcbiAgICAgICAgLy9cImRlY3J5cHRcIiBvciBcInVud3JhcEtleVwiIGZvciBwcml2YXRlIGtleSBpbXBvcnRzXG4gICAgKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICAvL3JldHVybnMgYSBwdWJsaWNLZXkgKG9yIHByaXZhdGVLZXkgaWYgeW91IGFyZSBpbXBvcnRpbmcgYSBwcml2YXRlIGtleSlcbiAgICAgICAgICAgIHJldHVybiBrZXlcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiZXJyb3IgZGVjcnlwdGluZyBmcm9tIGRlY3J5cHRlZCBidWZmZXJcIiArIGUpXG4gICAgICAgIH0pXG4gICAgcmV0dXJuIHByaXZhdGVLZXlDcnlwdG9cbn1cbmNyeXB0LmdldEVQdWJsaWNLZXlGcm9tQnVmZmVyID0gYXN5bmMgKHB1YmxpY0tleUJ1ZmZlcikgPT4ge1xuICAgIHZhciBwdWJsaWNLZXlDcnlwdG8gPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXG4gICAgICAgIFwic3BraVwiLCAvL2NhbiBiZSBcImp3a1wiIChwdWJsaWMgb3IgcHJpdmF0ZSksIFwic3BraVwiIChwdWJsaWMgb25seSksIG9yIFwicGtjczhcIiAocHJpdmF0ZSBvbmx5KVxuICAgICAgICBwdWJsaWNLZXlCdWZmZXIsLy9wYXNzIGFycmF5YnVmZmVyIG9mIHRoZSBwdWJsaWMga2V5XG4gICAgICAgIHsgICAvL3RoZXNlIGFyZSB0aGUgYWxnb3JpdGhtIG9wdGlvbnNcbiAgICAgICAgICAgIG5hbWU6IFwiUlNBLU9BRVBcIixcbiAgICAgICAgICAgIGhhc2g6IHsgbmFtZTogXCJTSEEtMjU2XCIgfSwgLy9jYW4gYmUgXCJTSEEtMVwiLCBcIlNIQS0yNTZcIiwgXCJTSEEtMzg0XCIsIG9yIFwiU0hBLTUxMlwiXG4gICAgICAgIH0sXG4gICAgICAgIHRydWUsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgW1wiZW5jcnlwdFwiXSAvL1wiZW5jcnlwdFwiIG9yIFwid3JhcEtleVwiIGZvciBwdWJsaWMga2V5IGltcG9ydCBvclxuICAgICAgICAvL1wiZGVjcnlwdFwiIG9yIFwidW53cmFwS2V5XCIgZm9yIHByaXZhdGUga2V5IGltcG9ydHNcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhIHB1YmxpY0tleSAob3IgcHJpdmF0ZUtleSBpZiB5b3UgYXJlIGltcG9ydGluZyBhIHByaXZhdGUga2V5KVxuICAgICAgICAgICAgcmV0dXJuIGtleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGUpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSwgZS5zdGFjaylcbiAgICAgICAgfSlcbiAgICByZXR1cm4gcHVibGljS2V5Q3J5cHRvXG59XG5jcnlwdC5nZXRTUHVibGljS2V5RnJvbUJ1ZmZlciA9IGFzeW5jIChwdWJsaWNLZXlCdWZmZXIpID0+IHtcbiAgICB2YXIgZVB1YmxpY0tleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcbiAgICAgICAgXCJzcGtpXCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXG4gICAgICAgIHB1YmxpY0tleUJ1ZmZlcixcbiAgICAgICAgeyAgIC8vdGhlc2UgYXJlIHRoZSBhbGdvcml0aG0gb3B0aW9uc1xuICAgICAgICAgICAgbmFtZTogXCJSU0FTU0EtUEtDUzEtdjFfNVwiLFxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcbiAgICAgICAgfSxcbiAgICAgICAgdHJ1ZSwgLy93aGV0aGVyIHRoZSBrZXkgaXMgZXh0cmFjdGFibGUgKGkuZS4gY2FuIGJlIHVzZWQgaW4gZXhwb3J0S2V5KVxuICAgICAgICBbXCJ2ZXJpZnlcIl0gLy9cInZlcmlmeVwiIGZvciBwdWJsaWMga2V5IGltcG9ydCwgXCJzaWduXCIgZm9yIHByaXZhdGUga2V5IGltcG9ydHNcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChwdWJsaWNLZXkpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhIHB1YmxpY0tleSAob3IgcHJpdmF0ZUtleSBpZiB5b3UgYXJlIGltcG9ydGluZyBhIHByaXZhdGUga2V5KVxuICAgICAgICAgICAgcmV0dXJuIHB1YmxpY0tleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4gZVB1YmxpY0tleUNyeXB0b1xufVxuY3J5cHQuZW5jcnlwdEFlc0tleSA9IGFzeW5jIChhZXNLZXlCdWZmZXIsIHB1YmxpY0tleUNyeXB0bykgPT4ge1xuICAgIC8vIHBrIGlzIHByaXZhdGVrZXkgQ3J5dG9LZXkgb2JqZWN0XG4gICAgLy9nZW5lcmF0ZSByYW5kb20gYWVzIGtleSB0aGVuIGVuY3J5cHQgdGhlIGFlcyBrZXkgd2l0aCB1c2VyJ3MgKG93bmVyKSBwdWJsaWMga2V5IFxuICAgIHZhciBlbmNyeXB0ZWRBZXNLZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5lbmNyeXB0KFxuICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIlJTQS1PQUVQXCIsXG4gICAgICAgICAgICAvL2xhYmVsOiBVaW50OEFycmF5KFsuLi5dKSAvL29wdGlvbmFsXG4gICAgICAgIH0sXG4gICAgICAgIHB1YmxpY0tleUNyeXB0bywgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxuICAgICAgICBhZXNLZXlCdWZmZXIgLy9BcnJheUJ1ZmZlciBvZiBkYXRhIHlvdSB3YW50IHRvIGVuY3J5cHRcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChlbmNyeXB0ZWQpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhbiBBcnJheUJ1ZmZlciBjb250YWluaW5nIHRoZSBlbmNyeXB0ZWQgZGF0YVxuICAgICAgICAgICAgcmV0dXJuIGVuY3J5cHRlZFxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4gZW5jcnlwdGVkQWVzS2V5Ly9lbmNyeXB0IGFlcyBrZXkgd2l0aCBwdWJsaWMga2V5IFxufVxuY3J5cHQuZGVjcnlwdEFlc0tleSA9IGFzeW5jIChhZXNLZXlCdWZmZXIsIHByaXZhdGVLZXlDcnlwdG8pID0+IHtcbiAgICAvLyBwayBpcyBwcml2YXRla2V5IENyeXRvS2V5IG9iamVjdFxuICAgIC8vZ2VuZXJhdGUgcmFuZG9tIGFlcyBrZXkgdGhlbiBlbmNyeXB0IHRoZSBhZXMga2V5IHdpdGggdXNlcidzIChvd25lcikgcHVibGljIGtleSBcbiAgICB2YXIgZGVjcnlwdGVkQWVzS2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZGVjcnlwdChcbiAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJSU0EtT0FFUFwiLFxuICAgICAgICAgICAgLy9sYWJlbDogVWludDhBcnJheShbLi4uXSkgLy9vcHRpb25hbFxuICAgICAgICB9LFxuICAgICAgICBwcml2YXRlS2V5Q3J5cHRvLCAvL2Zyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5IGFib3ZlXG4gICAgICAgIGFlc0tleUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gZGVjcnlwdFxuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGRlY3J5cHRlZCkge1xuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGVuY3J5cHRlZCBkYXRhXG4gICAgICAgICAgICByZXR1cm4gZGVjcnlwdGVkXG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHJldHVybiBkZWNyeXB0ZWRBZXNLZXkvL2VuY3J5cHQgYWVzIGtleSB3aXRoIHB1YmxpYyBrZXkgXG59XG5jcnlwdC5kZWNyeXB0X0FFUyA9IGFzeW5jIChwcml2YXRlS2V5U3RyaW5nLCBwYXNzd29yZCkgPT4ge1xuICAgIHZhciBpdiA9IG5ldyBVaW50OEFycmF5KFsxNTQsIDE4OCwgOTMsIDY1LCA4NywgMTM4LCAzNywgMjU0LCAyNDAsIDc4LCAxMCwgMTA1XSlcbiAgICB2YXIga2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuaW1wb3J0S2V5KFxuICAgICAgICBcInJhd1wiLCAvL29ubHkgXCJyYXdcIiBpcyBhbGxvd2VkXG4gICAgICAgIGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLCAvL3lvdXIgdXNlcidzIHBhc3N3b3JkXG4gICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiUEJLREYyXCIsXG4gICAgICAgIH0sXG4gICAgICAgIGZhbHNlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXG4gICAgICAgIFtcImRlcml2ZUtleVwiXSAvL2NhbiBiZSBhbnkgY29tYmluYXRpb24gb2YgXCJkZXJpdmVLZXlcIiBhbmQgXCJkZXJpdmVCaXRzXCJcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiBrZXlcbiAgICAgICAgICAgIC8vcmV0dXJuIGltcG9ydGVkIGtleSBvYmplY3QgZnJvbSBwYXNzd29yZFx0XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHZhciBlbmtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmRlcml2ZUtleShcbiAgICAgICAge1xuICAgICAgICAgICAgXCJuYW1lXCI6IFwiUEJLREYyXCIsXG4gICAgICAgICAgICBzYWx0OiBoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKHBhc3N3b3JkKSxcbiAgICAgICAgICAgIGl0ZXJhdGlvbnM6IDEwMDAsXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTFcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcbiAgICAgICAgfSxcbiAgICAgICAga2V5LCAvL3lvdXIga2V5IGZyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5XG4gICAgICAgIHsgLy90aGUga2V5IHR5cGUgeW91IHdhbnQgdG8gY3JlYXRlIGJhc2VkIG9uIHRoZSBkZXJpdmVkIGJpdHNcbiAgICAgICAgICAgIG5hbWU6IFwiQUVTLUdDTVwiLCAvL2NhbiBiZSBhbnkgQUVTIGFsZ29yaXRobSAoXCJBRVMtQ1RSXCIsIFwiQUVTLUNCQ1wiLCBcIkFFUy1DTUFDXCIsIFwiQUVTLUdDTVwiLCBcIkFFUy1DRkJcIiwgXCJBRVMtS1dcIiwgXCJFQ0RIXCIsIFwiREhcIiwgb3IgXCJITUFDXCIpXG4gICAgICAgICAgICAvL3RoZSBnZW5lcmF0ZUtleSBwYXJhbWV0ZXJzIGZvciB0aGF0IHR5cGUgb2YgYWxnb3JpdGhtXG4gICAgICAgICAgICBsZW5ndGg6IDEyOCwgLy9jYW4gYmUgIDEyOCwgMTkyLCBvciAyNTZcbiAgICAgICAgfSxcbiAgICAgICAgdHJ1ZSwgLy93aGV0aGVyIHRoZSBkZXJpdmVkIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXG4gICAgICAgIFtcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCJdIC8vbGltaXRlZCB0byB0aGUgb3B0aW9ucyBpbiB0aGF0IGFsZ29yaXRobSdzIGltcG9ydEtleVxuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgLy9yZXR1cm5zIHRoZSBkZXJpdmVkIGtleVxuICAgICAgICAgICAgcmV0dXJuIGtleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICAvL3N0ZXAgMjogdXNlIGtleSB0byBkZWNyeXB0XG4gICAgdmFyIGRlY3J5cHRlZF9kYXRhID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZGVjcnlwdChcbiAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsXG4gICAgICAgICAgICBpdjogaXYsIC8vVGhlIGluaXRpYWxpemF0aW9uIHZlY3RvciB5b3UgdXNlZCB0byBlbmNyeXB0XG4gICAgICAgICAgICAvL2FkZGl0aW9uYWxEYXRhOiBBcnJheUJ1ZmZlciwgLy9UaGUgYWRkdGlvbmFsRGF0YSB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXG4gICAgICAgICAgICAvL3RhZ0xlbmd0aDogMTI4LCAvL1RoZSB0YWdMZW5ndGggeW91IHVzZWQgdG8gZW5jcnlwdCAoaWYgYW55KVxuICAgICAgICB9LFxuICAgICAgICBlbmtleSwgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxuICAgICAgICAvL2hlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocHJpdmF0ZUtleVN0cmluZykgLy9BcnJheUJ1ZmZlciBvZiB0aGUgZGF0YSBmb3IgZHVtbXkgZGF0YVxuICAgICAgICBwcml2YXRlS2V5U3RyaW5nLy8gZm9yIGR1bW15IGRhdGEgYXMgaXQgaXMgYWxyZWFkeSBhbiBhcnJheSBidWZmZXJcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkZWNyeXB0ZWQpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhbiBBcnJheUJ1ZmZlciBjb250YWluaW5nIHRoZSBkZWNyeXB0ZWQgZGF0YVxuICAgICAgICAgICAgcmV0dXJuIGRlY3J5cHRlZFxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4gZGVjcnlwdGVkX2RhdGFcbn1cbmNyeXB0LmVuY3J5cHRGaWxlQWVzID0gYXN5bmMgKGZpbGVCdWZmZXIsIGFlc0tleSwgaXYpID0+IHtcbiAgICBsZXQgZW5jcnlwdGVkRmlsZUJ1ZmZlciA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmVuY3J5cHQoXG4gICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6IFwiQUVTLUdDTVwiLFxuXG4gICAgICAgICAgICAvL0Rvbid0IHJlLXVzZSBpbml0aWFsaXphdGlvbiB2ZWN0b3JzIVxuICAgICAgICAgICAgLy9BbHdheXMgZ2VuZXJhdGUgYSBuZXcgaXYgZXZlcnkgdGltZSB5b3VyIGVuY3J5cHQhXG4gICAgICAgICAgICAvL1JlY29tbWVuZGVkIHRvIHVzZSAxMiBieXRlcyBsZW5ndGhcbiAgICAgICAgICAgIC8vaXY6IHdpbmRvdy5jcnlwdG8uZ2V0UmFuZG9tVmFsdWVzKG5ldyBVaW50OEFycmF5KDEyKSksXG4gICAgICAgICAgICBpdjogaXYsXG4gICAgICAgICAgICAvL1RhZyBsZW5ndGggKG9wdGlvbmFsKVxuICAgICAgICAgICAgdGFnTGVuZ3RoOiAxMjgsIC8vY2FuIGJlIDMyLCA2NCwgOTYsIDEwNCwgMTEyLCAxMjAgb3IgMTI4IChkZWZhdWx0KVxuICAgICAgICB9LFxuICAgICAgICBhZXNLZXksIC8vZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXkgYWJvdmVcbiAgICAgICAgZmlsZUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gZW5jcnlwdFxuICAgIClcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGVuY3J5cHRlZCkge1xuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGVuY3J5cHRlZCBkYXRhXG4gICAgICAgICAgICByZXR1cm4gZW5jcnlwdGVkXG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KVxuICAgIHJldHVybiBlbmNyeXB0ZWRGaWxlQnVmZmVyXG59XG5jcnlwdC5kZWNyeXB0RmlsZUFlcyA9IGFzeW5jIChmaWxlQnVmZmVyLCBhZXNLZXksIGl2KSA9PiB7XG4gICAgbGV0IGRlY3J5cHRlZEZpbGVCdWZmZXIgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5kZWNyeXB0KFxuICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIixcbiAgICAgICAgICAgIGl2OiBpdiwgLy9UaGUgaW5pdGlhbGl6YXRpb24gdmVjdG9yIHlvdSB1c2VkIHRvIGVuY3J5cHRcbiAgICAgICAgICAgIC8vYWRkaXRpb25hbERhdGE6IEFycmF5QnVmZmVyLCAvL1RoZSBhZGR0aW9uYWxEYXRhIHlvdSB1c2VkIHRvIGVuY3J5cHQgKGlmIGFueSlcbiAgICAgICAgICAgIHRhZ0xlbmd0aDogMTI4LCAvL1RoZSB0YWdMZW5ndGggeW91IHVzZWQgdG8gZW5jcnlwdCAoaWYgYW55KVxuICAgICAgICB9LFxuICAgICAgICBhZXNLZXksIC8vZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXkgYWJvdmVcbiAgICAgICAgZmlsZUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIHRoZSBkYXRhXG4gICAgKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoZGVjcnlwdGVkKSB7XG4gICAgICAgICAgICAvL3JldHVybnMgYW4gQXJyYXlCdWZmZXIgY29udGFpbmluZyB0aGUgZGVjcnlwdGVkIGRhdGFcbiAgICAgICAgICAgIHJldHVybiBkZWNyeXB0ZWRcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXG4gICAgICAgIH0pXG4gICAgcmV0dXJuIGRlY3J5cHRlZEZpbGVCdWZmZXJcbn1cbmNyeXB0LmdldFNpZ25hdHVyZSA9IGFzeW5jIChidWZmZXIpID0+IHtcbiAgICAvLyBpZiAoV2luZG93LnNQcml2YXRlS2V5PT0gdW5kZWZpbmVkKXtcbiAgICAvLyBcdHRoaXMuZ2V0UHJpdmF0ZUtleShlbWFpbElkKVxuICAgIC8vIH1cbiAgICBsZXQgc2lnbmF0dXJlID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuc2lnbihcbiAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogXCJSU0FTU0EtUEtDUzEtdjFfNVwiLFxuICAgICAgICB9LFxuICAgICAgICBXaW5kb3cuc1ByaXZhdGVLZXksIC8vZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXkgYWJvdmVcbiAgICAgICAgYnVmZmVyLy9BcnJheUJ1ZmZlciBvZiBkYXRhIHlvdSB3YW50IHRvIHNpZ25cbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChzaWduYXR1cmUpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhbiBBcnJheUJ1ZmZlciBjb250YWluaW5nIHRoZSBzaWduYXR1cmVcbiAgICAgICAgICAgIHJldHVybiBzaWduYXR1cmVcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXG4gICAgICAgIH0pXG4gICAgcmV0dXJuIHNpZ25hdHVyZVxufVxuY3J5cHQuZXhwb3J0QWVzID0gYXN5bmMgKGFlc0tleSkgPT4ge1xuICAgIGxldCBrZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5leHBvcnRLZXkoXG4gICAgICAgIFwicmF3XCIsIC8vY2FuIGJlIFwiandrXCIgb3IgXCJyYXdcIlxuICAgICAgICBhZXNLZXkgLy9leHRyYWN0YWJsZSBtdXN0IGJlIHRydWVcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXlkYXRhKSB7XG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGV4cG9ydGVkIGtleSBkYXRhXG4gICAgICAgICAgICByZXR1cm4ga2V5ZGF0YVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4ga2V5XG59XG5jcnlwdC5pbXBvcnRBZXMgPSBhc3luYyAoYWVzS2V5QnVmZmVyKSA9PiB7XG4gICAgbGV0IGtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcbiAgICAgICAgXCJyYXdcIiwgLy9jYW4gYmUgXCJqd2tcIiBvciBcInJhd1wiXG4gICAgICAgIGFlc0tleUJ1ZmZlcixcbiAgICAgICAgeyAgIC8vdGhpcyBpcyB0aGUgYWxnb3JpdGhtIG9wdGlvbnNcbiAgICAgICAgICAgIG5hbWU6IFwiQUVTLUdDTVwiLFxuICAgICAgICB9LFxuICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXG4gICAgICAgIFtcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCJdIC8vY2FuIFwiZW5jcnlwdFwiLCBcImRlY3J5cHRcIiwgXCJ3cmFwS2V5XCIsIG9yIFwidW53cmFwS2V5XCJcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyB0aGUgc3ltbWV0cmljIGtleVxuICAgICAgICAgICAgcmV0dXJuIGtleVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICByZXR1cm4ga2V5XG59XG5jcnlwdC5lbmNyeXB0a2V5ID0gYXN5bmMgKHBhc3N3b3JkLCBrZXlCdWZmZXIpID0+IHtcbiAgICAvL2NyZWF0ZSBJbml0aWFsaXphdGlvbiB2ZWN0b3IgZm9yIGFlcyBrZXlcbiAgICBsZXQgaXYgPSB3aW5kb3cuY3J5cHRvLmdldFJhbmRvbVZhbHVlcyhuZXcgVWludDhBcnJheSgxNikpXG4gICAgLy9jcmVhdGUgYSBrZXkgZnJvbSB1c2VyJ3MgcGFzc3dvcmRcbiAgICBsZXQgcGFzc3dvcmRLZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXG4gICAgICAgIFwicmF3XCIsXG4gICAgICAgIGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLFxuICAgICAgICB7IFwibmFtZVwiOiBcIlBCS0RGMlwiIH0sXG4gICAgICAgIGZhbHNlLFxuICAgICAgICBbXCJkZXJpdmVLZXlcIl1cbiAgICApXG4gICAgbGV0IGVuY3J5cHRpb25LZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5kZXJpdmVLZXkoXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIlBCS0RGMlwiLFxuICAgICAgICAgICAgc2FsdDogaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihwYXNzd29yZCksXG4gICAgICAgICAgICBpdGVyYXRpb25zOiAxMDAwLFxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0xXCIgfVxuICAgICAgICB9LFxuICAgICAgICBwYXNzd29yZEtleSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJuYW1lXCI6IFwiQUVTLUdDTVwiLFxuICAgICAgICAgICAgXCJsZW5ndGhcIjogMTI4XG4gICAgICAgIH0sXG4gICAgICAgIHRydWUsXG4gICAgICAgIFtcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCJdXG4gICAgKVxuICAgIGxldCBlbmNyeXB0ZWRLZXlEYXRhID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZW5jcnlwdFxuICAgICAgICAoe1xuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsXG4gICAgICAgICAgICBpdjogaXZcbiAgICAgICAgfSxcbiAgICAgICAgICAgIGVuY3J5cHRpb25LZXksXG4gICAgICAgICAgICBrZXlCdWZmZXJcbiAgICAgICAgKVxuICAgIC8vbGV0IGVuY3J5cHRlZEtleUJ1ZmZlciA9IG5ldyBVaW50OEFycmF5KGVuY3J5cHRlZEtleURhdGEpXG4gICAgcmV0dXJuIHtcbiAgICAgICAgZW5jcnlwdGVkS2V5QnVmZmVyOiBlbmNyeXB0ZWRLZXlEYXRhLFxuICAgICAgICBpdjogaXZcbiAgICB9XG59XG5jcnlwdC5kZWNyeXB0S2V5ID0gYXN5bmMgKHBhc3N3b3JkLCBrZXlCdWZmZXIsIGl2KSA9PiB7XG4gICAgLy9jb25zb2xlLmxvZyhpdikgXG4gICAgLy9jb25zb2xlLmxvZyhrZXlCdWZmZXIpXG4gICAgLy9jb25zb2xlLmxvZyhwYXNzd29yZClcbiAgICAvL3JldHVybnMgYmluYXJ5IHN0cmVhbSBvZiBwcml2YXRlIGtleVxuICAgIHZhciBwYXNzd29yZEtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcbiAgICAgICAgXCJyYXdcIiwgLy9vbmx5IFwicmF3XCIgaXMgYWxsb3dlZFxuICAgICAgICBoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKHBhc3N3b3JkKSwgLy95b3VyIHVzZXIncyBwYXNzd29yZFxuICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIlBCS0RGMlwiLFxuICAgICAgICB9LFxuICAgICAgICBmYWxzZSwgLy93aGV0aGVyIHRoZSBrZXkgaXMgZXh0cmFjdGFibGUgKGkuZS4gY2FuIGJlIHVzZWQgaW4gZXhwb3J0S2V5KVxuICAgICAgICBbXCJkZXJpdmVLZXlcIl0gLy9jYW4gYmUgYW55IGNvbWJpbmF0aW9uIG9mIFwiZGVyaXZlS2V5XCIgYW5kIFwiZGVyaXZlQml0c1wiXG4gICAgKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICByZXR1cm4ga2V5XG4gICAgICAgICAgICAvL3JldHVybiBpbXBvcnRlZCBrZXkgb2JqZWN0IGZyb20gcGFzc3dvcmRcdFxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcbiAgICAgICAgfSlcbiAgICB2YXIgZGVjcnlwdGlvbktleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmRlcml2ZUtleShcbiAgICAgICAge1xuICAgICAgICAgICAgXCJuYW1lXCI6IFwiUEJLREYyXCIsXG4gICAgICAgICAgICBzYWx0OiBoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKHBhc3N3b3JkKSxcbiAgICAgICAgICAgIGl0ZXJhdGlvbnM6IDEwMDAsXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTFcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcbiAgICAgICAgfSxcbiAgICAgICAgcGFzc3dvcmRLZXksIC8veW91ciBrZXkgZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXlcbiAgICAgICAgeyAvL3RoZSBrZXkgdHlwZSB5b3Ugd2FudCB0byBjcmVhdGUgYmFzZWQgb24gdGhlIGRlcml2ZWQgYml0c1xuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsIC8vY2FuIGJlIGFueSBBRVMgYWxnb3JpdGhtIChcIkFFUy1DVFJcIiwgXCJBRVMtQ0JDXCIsIFwiQUVTLUNNQUNcIiwgXCJBRVMtR0NNXCIsIFwiQUVTLUNGQlwiLCBcIkFFUy1LV1wiLCBcIkVDREhcIiwgXCJESFwiLCBvciBcIkhNQUNcIilcbiAgICAgICAgICAgIC8vdGhlIGdlbmVyYXRlS2V5IHBhcmFtZXRlcnMgZm9yIHRoYXQgdHlwZSBvZiBhbGdvcml0aG1cbiAgICAgICAgICAgIGxlbmd0aDogMTI4LCAvL2NhbiBiZSAgMTI4LCAxOTIsIG9yIDI1NlxuICAgICAgICB9LFxuICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGRlcml2ZWQga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcbiAgICAgICAgW1wiZW5jcnlwdFwiLCBcImRlY3J5cHRcIl0gLy9saW1pdGVkIHRvIHRoZSBvcHRpb25zIGluIHRoYXQgYWxnb3JpdGhtJ3MgaW1wb3J0S2V5XG4gICAgKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGRlcml2ZWQga2V5XG4gICAgICAgICAgICByZXR1cm4ga2V5XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxuICAgICAgICB9KTtcbiAgICAvL3N0ZXAgMjogdXNlIGtleSB0byBkZWNyeXB0XG4gICAgdmFyIGRlY3J5cHRlZEtleURhdGEgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5kZWNyeXB0KFxuICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIixcbiAgICAgICAgICAgIGl2OiBpdiwgLy9UaGUgaW5pdGlhbGl6YXRpb24gdmVjdG9yIHlvdSB1c2VkIHRvIGVuY3J5cHRcbiAgICAgICAgICAgIC8vYWRkaXRpb25hbERhdGE6IEFycmF5QnVmZmVyLCAvL1RoZSBhZGR0aW9uYWxEYXRhIHlvdSB1c2VkIHRvIGVuY3J5cHQgKGlmIGFueSlcbiAgICAgICAgICAgIC8vdGFnTGVuZ3RoOiAxMjgsIC8vVGhlIHRhZ0xlbmd0aCB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXG4gICAgICAgIH0sXG4gICAgICAgIGRlY3J5cHRpb25LZXksIC8vZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXkgYWJvdmVcbiAgICAgICAgLy90aGlzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKHByaXZhdGVLZXlTdHJpbmcpIC8vQXJyYXlCdWZmZXIgb2YgdGhlIGRhdGEgZm9yIGR1bW15IGRhdGFcbiAgICAgICAga2V5QnVmZmVyLy8gZm9yIGR1bW15IGRhdGEgYXMgaXQgaXMgYWxyZWFkeSBhbiBhcnJheSBidWZmZXJcbiAgICApXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkZWNyeXB0ZWQpIHtcbiAgICAgICAgICAgIC8vcmV0dXJucyBhbiBBcnJheUJ1ZmZlciBjb250YWluaW5nIHRoZSBkZWNyeXB0ZWQgZGF0YVxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcImtleSBkZWNyeXB0ZWQ6IFwiKyBkZWNyeXB0ZWQpXG4gICAgICAgICAgICByZXR1cm4gZGVjcnlwdGVkXG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9lciB3aGlsZSBkZWNyeXB0aW5nIHByaXZhdGUga2V5OiBcIiArIGUpXG4gICAgICAgIH0pXG4gICAgLy9jb25zb2xlLmxvZyhkZWNyeXB0ZWRLZXlEYXRhKVxuICAgIHJldHVybiBkZWNyeXB0ZWRLZXlEYXRhXG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyeXB0IiwiY2xhc3MgSHR0cEVycm9yIGV4dGVuZHMgRXJyb3Ige1xuICAgIGNvbnN0cnVjdG9yKHJlc3BvbnNlKSB7XG4gICAgICAgIHN1cGVyKClcbiAgICAgICAgdGhpcy5uYW1lID0gJ0h0dHBFcnJvcidcbiAgICAgICAgdGhpcy5tZXNzYWdlID0gJ0ludmFsaWQgUmVzcG9uc2Ugc3RhdHVzOiAnICsgcmVzcG9uc2Uuc3RhdHVzXG4gICAgfVxufVxuY2xhc3MgUGVybWlzc2lvbkVycm9yIGV4dGVuZHMgRXJyb3Ige1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigpXG4gICAgICAgIHRoaXMubmFtZSA9ICdQZXJtaXNzaW9uRXJyb3InXG4gICAgICAgIHRoaXMubWVzc2FnZSA9ICdJbnZhbGlkIFBlcm1pc3Npb25zIGZvciB0aGUgcmVxdWVzdGVkIG1lZGJsb2NrJ1xuICAgIH1cbn1cbmNsYXNzIExvZ2luRXJyb3IgZXh0ZW5kcyBFcnJvciB7XG4gICAgY29uc3RydWN0b3IoKXtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5uYW1lID0gJ0xvZ2luRXJyb3InXG4gICAgdGhpcy5tZXNzYWdlID0gJ1BsZWFzZSBtYWtlIHN1cmUgeW91IGFyZSBsb2dnZWQgaW4gdG8gY29udGludWUuJ1xuICAgIH1cbn1cbmV4cG9ydCB7SHR0cEVycm9yLFBlcm1pc3Npb25FcnJvcixMb2dpbkVycm9yfSIsIlxuaW1wb3J0IHtIdHRwRXJyb3J9IGZyb20gJy4vZXJyb3JzLmpzJ1xudmFyIGhlbHBlcnMgPSB7fVxuaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyID0gKGJhc2U2NCkgPT4ge1xuICAgIHZhciBiaW5hcnlfc3RyaW5nID0gd2luZG93LmF0b2IoYmFzZTY0KVxuICAgIHZhciBsZW4gPSBiaW5hcnlfc3RyaW5nLmxlbmd0aFxuICAgIHZhciBieXRlcyA9IG5ldyBVaW50OEFycmF5KGxlbilcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIGJ5dGVzW2ldID0gYmluYXJ5X3N0cmluZy5jaGFyQ29kZUF0KGkpXG4gICAgfVxuICAgIHJldHVybiBieXRlcy5idWZmZXJcbn1cblxuaGVscGVycy5hcnJheVRvQmFzZTY0ID0gKGFycmF5KSA9PiB7XG4gICAgcmV0dXJuIGJ0b2EoU3RyaW5nLmZyb21DaGFyQ29kZS5hcHBseShudWxsLCBuZXcgVWludDhBcnJheShhcnJheSkpKVxufVxuaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlciA9IChzdHIpID0+IHsgLy9uZWVkZWQgZm9yIGltcG9ydGtleSBJL1BcbiAgICB2YXIgZW5jb2RlciA9IG5ldyBUZXh0RW5jb2RlcihcInV0Zi04XCIpXG4gICAgcmV0dXJuIGVuY29kZXIuZW5jb2RlKHN0cilcbn1cbmhlbHBlcnMuY29udmVydEFycmF5QnVmZmVydG9TdHJpbmcgPSAoYnVmZmVyKSA9PiB7XG4gICAgdmFyIGRlY29kZXIgPSBuZXcgVGV4dERlY29kZXIoXCJ1dGYtOFwiKVxuICAgIHJldHVybiBkZWNvZGVyLmRlY29kZShidWZmZXIpXG59XG4vLyBDYWxjdWxhdGVzIGhleGFkZWNpbWFsIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiBhcnJheSBidWZmZXJcbmhlbHBlcnMuaGV4U3RyaW5nID0gKGJ1ZmZlcikgPT4ge1xuICAgIGNvbnN0IGJ5dGVBcnJheSA9IG5ldyBVaW50OEFycmF5KGJ1ZmZlcilcbiAgICBjb25zdCBoZXhDb2RlcyA9IFsuLi5ieXRlQXJyYXldLm1hcCh2YWx1ZSA9PiB7XG4gICAgICAgIGNvbnN0IGhleENvZGUgPSB2YWx1ZS50b1N0cmluZygxNilcbiAgICAgICAgY29uc3QgcGFkZGVkSGV4Q29kZSA9IGhleENvZGUucGFkU3RhcnQoMiwgJzAnKVxuICAgICAgICByZXR1cm4gcGFkZGVkSGV4Q29kZVxuICAgIH0pXG4gICAgcmV0dXJuIGhleENvZGVzLmpvaW4oJycpXG59XG5cbmhlbHBlcnMucmVtb3ZlTGluZXMgPSAocGVtKSA9PiB7XG4gICAgdmFyIGxpbmVzID0gcGVtLnNwbGl0KCdcXG4nKVxuICAgIHZhciBlbmNvZGVkU3RyaW5nID0gJydcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IChsaW5lcy5sZW5ndGggLSAxKTsgaSsrKSB7XG4gICAgICAgIGVuY29kZWRTdHJpbmcgKz0gbGluZXNbaV0udHJpbSgpXG4gICAgfVxuICAgIHJldHVybiBlbmNvZGVkU3RyaW5nXG59XG5cbmhlbHBlcnMuc3BraVRvUEVNID0gKGtleUJ1ZmZlcikgPT4ge1xuICAgIC8vIHZhciBrZXlkYXRhQjY0UyA9IGhlbHBlcnMuY29udmVyQXJyYXlCdWZmZXJ0b1N0cmluZyhrZXlCdWZmZXIpIFxuICAgIC8vIHZhciBrZXlkYXRhYjY0ID0gdGhpcy5iNjRFbmNvZGVVbmljb2RlKGtleWRhdGFCNjRTKVxuICAgIC8vIHZhciBrZXlkYXRhQjY0UGVtID0gdGhpcy5mb3JtYXRBc1BlbShrZXlkYXRhQlx0NjQpLy9hZGQgbmV3IGxpbmVzIGFuZCBzcGxpdCBpbnRvIDY0XG4gICAgdmFyIGtleWRhdGFCNjQgPSBoZWxwZXJzLmFycmF5VG9CYXNlNjQoa2V5QnVmZmVyKVxuICAgIHZhciBrZXlkYXRhQjY0UGVtID0gaGVscGVycy5mb3JtYXRBc1BlbShrZXlkYXRhQjY0KVxuICAgIHJldHVybiBrZXlkYXRhQjY0UGVtXG59XG5cbmhlbHBlcnMuZm9ybWF0QXNQZW0gPSAoc3RyKSA9PiB7XG4gICAgdmFyIGZpbmFsU3RyaW5nID0gJy0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tXFxuJ1xuXG4gICAgd2hpbGUgKHN0ci5sZW5ndGggPiAwKSB7XG4gICAgICAgIGZpbmFsU3RyaW5nICs9IHN0ci5zdWJzdHJpbmcoMCwgNjQpICsgJ1xcbidcbiAgICAgICAgc3RyID0gc3RyLnN1YnN0cmluZyg2NClcbiAgICB9XG5cbiAgICBmaW5hbFN0cmluZyA9IGZpbmFsU3RyaW5nICsgXCItLS0tLUVORCBQVUJMSUMgS0VZLS0tLS1cIlxuXG4gICAgcmV0dXJuIGZpbmFsU3RyaW5nXG59XG5oZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IgPSAocmVzcG9uc2UpID0+IHtcbiAgICBpZiAocmVzcG9uc2Uuc3RhdHVzICE9PSAyMDAgJiYgcmVzcG9uc2Uuc3RhdHVzICE9PSAyMDIpIHtcbiAgICAgICAgLy9jb25zb2xlLmVycm9yKFwiUmVzcG9uc2UgT2JqZWN0OiBcIilcbiAgICAgICAgLy9jb25zb2xlLmVycm9yKFwiUmVzcG9uc2Ugb2JqZWN0OiBcIitKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpXG4gICAgICAgIHRocm93IG5ldyBIdHRwRXJyb3IocmVzcG9uc2UpXG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgaGVscGVycyIsImltcG9ydCBhc3NldCBmcm9tICcuL2Fzc2V0cy5qcyc7XG5pbXBvcnQgaGVscGVycyBmcm9tICcuL2hlbHBlcnMuanMnXG5pbXBvcnQgY3J5cHQgZnJvbSAnLi9jcnlwdG8uanMnXG5pbXBvcnQge1Blcm1pc3Npb25FcnJvcixIdHRwRXJyb3J9IGZyb20gJy4vZXJyb3JzLmpzJztcbihmdW5jdGlvbiAoKSB7XG4gICAgLyoqYXNzZXRcbiAgICAgKiBTRUNUSU9OOiBtZWRibG9ja3MuanNcbiAgICAgKi9cbiAgICBcbiAgICAvKlxuICAgICogTWFpbiBNZWRibG9ja3MgY2xhc3NcbiAgICAqL1xuXG4gICAgY2xhc3MgTWVkYmxvY2tzIHtcbiAgICAgICAgY29uc3RydWN0b3IobG9jYWxOb2RlSVAgPSBcIjMuOTUuMjAwLjEzXCIsbG9jYWxOb2RlUG9ydCA9IFwiODA4MFwiLGxvY2FsSXBmc0lwID0gXCIzLjk1LjIwMC4xM1wiLGxvY2FsSXBmc1BvcnQgPSBcIjUwMDFcIikge1xuICAgICAgICAgICAgdGhpcy5sb2NhbE5vZGVJUCA9IGxvY2FsTm9kZUlQLCBcbiAgICAgICAgICAgIHRoaXMubG9jYWxOb2RlUG9ydCA9IGxvY2FsTm9kZVBvcnQsXG4gICAgICAgICAgICB0aGlzLmxvY2FsSXBmc0lwID0gbG9jYWxJcGZzSXAsXG4gICAgICAgICAgICB0aGlzLmxvY2FsSXBmc1BvcnQgPSBsb2NhbElwZnNQb3J0XG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgYWRkUGVybWlzc2lvbihoYXNoLHNlbmRlckVtYWlsSWQscmVjZWl2ZXJFbWFpbElkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAscG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydClcbiAgICAgICAge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAvLzE6IEdldCBCbG9jayBmcm9tIGJsb2NrY2hhaW4gYW5kIGNoZWNrIGlmIHRoZSBsb2dnZWRpbiB1c2VyIGlzIHRoZSBvd25lclxuICAgICAgICAgICAgbGV0IGJsb2NrID0gYXdhaXQgdGhpcy5nZXRCbG9jayhoYXNoLG5vZGVJcCxwb3J0KVxuICAgICAgICAgICAgLy8yOkRlY3J5cHQgdGhlIHJlY2VpdmVyS2V5IHRvIGdldCBhZXMga2V5XG4gICAgICAgICAgICBsZXQgcmVjZWl2ZXJLZXlTXG4gICAgICAgICAgICBmb3IgKGxldCBpbmRleD0wIDsgaW5kZXggPCBibG9jay5wZXJtaXNzaW9ucy5sZW5ndGggOyBpbmRleCsrKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmKGJsb2NrLnBlcm1pc3Npb25zW2luZGV4XS5yZWNlaXZlckVtYWlsSWQ9PSBzZW5kZXJFbWFpbElkKXtcbiAgICAgICAgICAgICAgICAgICAgcmVjZWl2ZXJLZXlTPSBibG9jay5wZXJtaXNzaW9uc1tpbmRleF0ucmVjZWl2ZXJLZXlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0cnlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgIGlmIChyZWNlaXZlcktleVM9PXVuZGVmaW5lZClcbiAgICAgICAgICAgIHsgICBcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgUGVybWlzc2lvbkVycm9yKCkgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaChlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSlcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlLm1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsZXQgcmVjZWl2ZXJLZXlCID0gaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKHJlY2VpdmVyS2V5UylcbiAgICAgICAgICAgIGxldCBhZXNLZXlCdWZmZXIgPSBhd2FpdCBjcnlwdC5kZWNyeXB0QWVzS2V5KHJlY2VpdmVyS2V5QixXaW5kb3cuZVByaXZhdGVLZXkpXG4gICAgICAgICAgICBsZXQgYWVzS2V5ID0gYXdhaXQgY3J5cHQuaW1wb3J0QWVzKGFlc0tleUJ1ZmZlcilcbiAgICAgICAgICAgIC8vbGV0IHJlY2VpdmVyS2V5ID0gYmxvY2sucGVybWlzc2lvbnNbMF1bXCJyZWNlaXZlcktleVwiXSB3b3Jrc1xuICAgICAgICAgICAgbGV0IHJlY2VpdmVyS2V5QnVmZmVyID0gKGF3YWl0IGNyeXB0LmVuY3J5cHRBZXNLZXkoYXdhaXQgY3J5cHQuZXhwb3J0QWVzKGFlc0tleSksIGF3YWl0IHRoaXMuZ2V0UHVibGljS2V5KHJlY2VpdmVyRW1haWxJZCxub2RlSXAscG9ydCkpKVxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIrbm9kZUlwK1wiOlwiK3BvcnQrXCIvYmxvY2svcGVybWlzc2lvbnNcIiBcbiAgICAgICAgICAgIGxldCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgIGlwZnNIYXNoOiBoYXNoLFxuICAgICAgICAgICAgICAgIHNlbmRlckVtYWlsSWQ6IHNlbmRlckVtYWlsSWQsXG4gICAgICAgICAgICAgICAgcGVybWlzc2lvbnM6ICBbXG4gICAgICAgICAgICAgICAgICAgIHsgcmVjZWl2ZXJFbWFpbElkOiByZWNlaXZlckVtYWlsSWQsIHJlY2VpdmVyS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQocmVjZWl2ZXJLZXlCdWZmZXIpIH1cblxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0pIFxuICAgICAgICAgICAgbGV0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcbiAgICAgICAgICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4gaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKSlcbiAgICAgICAgICAgIC5jYXRjaCggKGUpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXG4gICAgICAgICAgICAgICAgcmV0dXJufSlcblxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBhc3luYyBnZXRQdWJsaWNLZXkoZW1haWxJZCxub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KS8vIGZvciBhZGRibG9jayBnZXR0aW5nIHB1YmxpYyBrZXlzIG9mIG90aGVyIHVzZXJzXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrbm9kZVBvcnQrXCIvdXNlci9cIitlbWFpbElkIFxuICAgICAgICAgICAgdmFyIGVQdWJsaWNLZXkgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpID0+IHtoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF3YWl0IHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKFxuICAgICAgICAgICAgICAgICAgICBhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhhd2FpdCBjcnlwdC5nZXRFUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uZVB1YmxpY0tleSkpKSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhd2FpdCBjcnlwdC5nZXRFUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uZVB1YmxpY0tleSkpKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy9yZXR1cm4gYXdhaXQgY3J5cHQuZ2V0RVB1YmxpY0tleUZyb21CdWZmZXIoaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKGhlbHBlcnMucmVtb3ZlTGluZXMocmVzcG9uc2VKc29uLmVQdWJsaWNLZXkpKSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIG9jY2VyZWQgd2hpbGUgZmV0Y2hpbmcgcHVibGljIGtleXMgOlwiLGUpXG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgcmV0dXJuIGVQdWJsaWNLZXlcbiAgICAgICAgICAgIFxuICAgICAgICB9XG5cbiAgICAgICAgYXN5bmMgcmVnaXN0ZXIoZW1haWwscGFzc3dvcmQsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0LG5hbWUsc2V4KS8vZVB1YmxpY0tleSwgc1B1YmxpY0tleSAsIGVtYWlsaWQsIG1ldGFkYXRhW09wdGlvbmFsXVxuICAgICAgICAvLyBObyBtb3JlIHBhc3N3b3JkIGVuY3J5cHRpb24gb2YgcHJpdmF0ZWtleVxuICAgICAgICB7ICAgXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhuYW1lKVxuICAgICAgICAgICAgY29uc29sZS5sb2coc2V4KVxuICAgICAgICAgICAgLy92YXIgaXY9XCJpdlwiXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nIChcIlVzZXIgZW50ZXJlZCBwYXNzd29yZDogXCIgKyBwYXNzd29yZCkgXG4gICAgICAgICAgICB2YXIgZUtleVBhaXIgPSBhd2FpdCBjcnlwdC5nZW5lcmF0ZV9SU0FfS2V5cGFpcigpIFxuICAgICAgICAgICAgdmFyIHNLZXlQYWlyID0gYXdhaXQgY3J5cHQuZ2VuZXJhdGVfUlNBU1NBX0tleVBhaXIoKSBcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJHZW5lcmF0ZWQgU2lnbmF0dXJlIGtleSBwYWlyOiBcXG5cIilcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coc0tleVBhaXIpXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiR2VuZXJhdGVkIEVuY3J5cHRpb24ga2V5IHBhaXI6IFxcblwiKVxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlS2V5UGFpcikgXG4gICAgICAgICAgICAvL2h0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzk1NzUzNy9ob3ctY2FuLWktZGlzcGxheS1hLWphdmFzY3JpcHQtb2JqZWN0XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGVLZXlQYWlyLmtleVBhaXJDcnlwdG8ucHJpdmF0ZUtleUNyeXB0bylcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coZUtleVBhaXIua2V5UGFpckNyeXB0by5wcml2YXRlS2V5Q3J5cHRvKVxuICAgICAgICAgICAgV2luZG93LmVQcml2YXRlS2V5ID0gZUtleVBhaXIua2V5UGFpckNyeXB0by5wcml2YXRlS2V5Q3J5cHRvXG4gICAgICAgICAgICBXaW5kb3cuc1ByaXZhdGVLZXkgPSBzS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnByaXZhdGVLZXlDcnlwdG9cbiAgICAgICAgICAgIFdpbmRvdy5lUHVibGljS2V5ID0gZUtleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG9cbiAgICAgICAgICAgIFdpbmRvdy5zUHVibGljS2V5ID0gc0tleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG9cbiAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnN0b3JlUHVibGljS2V5cyhlbWFpbCxlS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnB1YmxpY0tleUNyeXB0bywgc0tleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG8sbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICAgICAgYXdhaXQgdGhpcy5zdG9yZUtleXMoZW1haWwsZUtleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG8sc0tleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG8sIGVLZXlQYWlyLmtleVBhaXJDcnlwdG8ucHJpdmF0ZUtleUNyeXB0bywgc0tleVBhaXIua2V5UGFpckNyeXB0by5wcml2YXRlS2V5Q3J5cHRvLHBhc3N3b3JkLG5vZGVJcCxub2RlUG9ydCkgXG4gICAgICAgICAgICBsZXQgdXNlcklkZW50aXR5UGFyYW1zID0ge1xuICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUsXG4gICAgICAgICAgICAgICAgc2V4OiBzZXgsXG4gICAgICAgICAgICAgICAgZW1haWxpZDplbWFpbFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc29sZS5sb2codXNlcklkZW50aXR5UGFyYW1zKVxuICAgICAgICAgICAgYXdhaXQgdGhpcy5sb2dpbihlbWFpbCxwYXNzd29yZCxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgICAgICAvL2NyZWF0ZSBhIGZpbGUgZm9yIHVzZXIgaW5kZW50aXR5IG1hbmFnZW1lbnQgXG4gICAgICAgICAgICBsZXQgdXNlcklkZW50aXR5RmlsZSA9IG5ldyBGaWxlKFtKU09OLnN0cmluZ2lmeSh1c2VySWRlbnRpdHlQYXJhbXMpXSxcInVzZXJJZGVudGl0eUZpbGVcIix7dHlwZTogXCJ0ZXh0L3BsYWluXCJ9KVxuICAgICAgICAgICAgLy91cGxvYWQgdGhlIGZpbGUgdG8gaXBmc1xuICAgICAgICAgICAgdmFyIGEgPSBhd2FpdCB0aGlzLnVwbG9hZEZpbGUodXNlcklkZW50aXR5RmlsZSxlbWFpbCxlbWFpbCx0cnVlLG5vZGVJcCxub2RlUG9ydCx0aGlzLmxvY2FsSXBmc0lwLHRoaXMubG9jYWxJcGZzUG9ydClcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGEpXG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBhc3luYyBzdG9yZVB1YmxpY0tleXMoZW1haWxJZCxlUHVibGljS2V5Q3J5cHRvLCBzUHVibGljS2V5Q3J5cHRvLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpLy93b3Jrc1xuICAgICAgICB7Ly9CYWNrZW5kIHJlZ2lzdHJhdGlvblxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIiArIG5vZGVQb3J0ICsgXCIvdXNlclwiIFxuICAgICAgICAgICAgLy9sZXQgZW5jcnlwdGVkU2tleSA9IGF3YWl0IGNyeXB0LmVuY3J5cHRrZXkocGFzc3dvcmQsYXdhaXQgY3J5cHQuZ2V0UGtjczgoc1B1YmxpY0tleUNyeXB0bykpXG4gICAgICAgICAgICBjb25zdCBkYXRhID1cbiAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICAgICAgICBlbWFpbElkOiBlbWFpbElkLFxuICAgICAgICAgICAgICAgIGVQdWJsaWNLZXk6IGF3YWl0IGNyeXB0LmdldFBlbShlUHVibGljS2V5Q3J5cHRvKSxcbiAgICAgICAgICAgICAgICBzUHVibGljS2V5OiBhd2FpdCBjcnlwdC5nZXRQZW0oc1B1YmxpY0tleUNyeXB0bylcbiAgICAgICAgICAgIH0gKVxuICAgICAgICAgICAgbGV0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKVxuICAgICAgICAgICAgY29uc3QgYmxvY2tkYXRhID1cbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxuICAgICAgICAgICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYmxvY2tkYXRhKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSk9PntcbiAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpLnRoZW4oKHJlc3BKc29uKT0+KFxuICAgICAgICAgICAgICAgICAgICBhbGVydChKU09OLnN0cmluZ2lmeShyZXNwSnNvbi5lbWFpbElkLCBudWxsLCAyKSkpXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaCggZSA9PiB7IFxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIHdoaWxlIHN0b3JpbmcgcHVibGljIGtleXM6ICcrZSlcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgc3RvcmVLZXlzKGVtYWlsSWQsIGVQdWJsaWNLZXlDcnlwdG8sIHNQdWJsaWNLZXlDcnlwdG8sIGVQcml2YXRlS2V5Q3J5cHRvLCBzUHJpdmF0ZUtleUNyeXB0byxwYXNzd29yZCxub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KS8vd29ya3NcbiAgICAgICAge1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlUHJpdmF0ZUtleUNyeXB0bylcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coc1ByaXZhdGVLZXlDcnlwdG8pXG4gICAgICAgICAgICAvL2VjbnJ5cHQgdGhlIHByaXZhdGVrZXlzIHdpdGggdXNlcidzIHBhc3N3b3JkIFxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIiArIG5vZGVQb3J0ICsgXCIva2V5XCIgXG4gICAgICAgICAgICBsZXQgZW5jcnlwdGVkRUtleSA9IGF3YWl0IGNyeXB0LmVuY3J5cHRrZXkocGFzc3dvcmQsYXdhaXQgY3J5cHQuZ2V0UGtjczgoZVByaXZhdGVLZXlDcnlwdG8pKVxuICAgICAgICAgICAgbGV0IGVuY3J5cHRlZFNrZXkgPSBhd2FpdCBjcnlwdC5lbmNyeXB0a2V5KHBhc3N3b3JkLGF3YWl0IGNyeXB0LmdldFBrY3M4KHNQcml2YXRlS2V5Q3J5cHRvKSlcbiAgICAgICAgICAgIGNvbnN0IGJvZHkgPVxuICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgZW1haWxJZDogZW1haWxJZCxcbiAgICAgICAgICAgICAgICBzUHVibGljS2V5OiBhd2FpdCBjcnlwdC5nZXRQZW0oc1B1YmxpY0tleUNyeXB0byksXG4gICAgICAgICAgICAgICAgZVB1YmxpY0tleTogYXdhaXQgY3J5cHQuZ2V0UGVtKGVQdWJsaWNLZXlDcnlwdG8pLFxuICAgICAgICAgICAgICAgIHNQcml2YXRlS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkU2tleS5lbmNyeXB0ZWRLZXlCdWZmZXIpLC8vcmV0dXJucyBhIHN0cmluZ1xuICAgICAgICAgICAgICAgIGVQcml2YXRlS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkRUtleS5lbmNyeXB0ZWRLZXlCdWZmZXIpLFxuICAgICAgICAgICAgICAgIElWOntcbiAgICAgICAgICAgICAgICAgICAgaXZlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkRUtleS5pdiksXG4gICAgICAgICAgICAgICAgICAgIGl2czogaGVscGVycy5hcnJheVRvQmFzZTY0KGVuY3J5cHRlZFNrZXkuaXYpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSkpXG4gICAgICAgICAgICAuY2F0Y2goIGU9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igd2hpbGUgc3Ryb2luZyBwcml2YXRlIGtleXMgOicrZSlcbiAgICAgICAgICAgICAgICByZXR1cm59KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIGxvZ2luKGVtYWlsLHBhc3N3b3JkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpIHtcbiAgICAgICAgICAgIC8vY3JlYXRlIHNlc3Npb24gdmFyaWFibGVzIGZvciB0aGUgdXNlclxuICAgICAgICAgICAgV2luZG93Lm1iVXNlciA9IGVtYWlsXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmdldFByaXZhdGVLZXlzKGVtYWlsLHBhc3N3b3JkLG5vZGVJcCxub2RlUG9ydClcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2V0UHVibGljS2V5cyhlbWFpbCxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgbnVrZShub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KSB7XG5cdCAgICBsb2dvdXQoKTtcbiBcdFx0Y29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL251a2VcIlxuICAgICAgICAgICAgZmV0Y2godXJsLCB7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIlxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtyZXNwb25zZS50ZXh0KCkudGhlbigocmopID0+IGFsZXJ0KHJqKSl9KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIGdldFB1YmxpY0tleXMoZW1haWxJZCxub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KS8vZmV0Y2ggcHVibGljIGtleSBhbmQgc2V0IHNlc3Npb24gdmFyaWFibGVcbiAgICAgICAge1xuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL3VzZXIvXCIrZW1haWxJZCBcbiAgICAgICAgICAgIGZldGNoKHVybCwge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJHRVRcIlxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IFxuICAgICAgICAgICAgICAgIHsgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxuICAgICAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBXaW5kb3cuZVB1YmxpY0tleSA9IGF3YWl0IGNyeXB0LmdldEVQdWJsaWNLZXlGcm9tQnVmZmVyKGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihoZWxwZXJzLnJlbW92ZUxpbmVzKHJlc3BvbnNlSnNvbi5lUHVibGljS2V5KSkpXG4gICAgICAgICAgICAgICAgICAgICAgICBXaW5kb3cuc1B1YmxpY0tleSA9IGF3YWl0IGNyeXB0LmdldEVQdWJsaWNLZXlGcm9tQnVmZmVyKGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihoZWxwZXJzLnJlbW92ZUxpbmVzKHJlc3BvbnNlSnNvbi5zUHVibGljS2V5KSkpXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhXaW5kb3cuZVB1YmxpY0tleSlcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFdpbmRvdy5zUHVibGljS2V5KVxuICAgICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGdldHRpbmcgcHVibGljIGtleXM6ICcrZSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgICBhc3luYyBnZXRQcml2YXRlS2V5cyhlbWFpbElkLHBhc3N3b3JkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiK25vZGVJcCArXCI6XCIrbm9kZVBvcnQrXCIva2V5L1wiK2VtYWlsSWQgXG4gICAgICAgICAgICBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgcmVzcG9uc2UuanNvbigpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGF3YWl0IGNyeXB0LmRlY3J5cHRLZXkocGFzc3dvcmQsaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKHJlc3BvbnNlSnNvbi5lUHJpdmF0ZUtleSksaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKEpTT04ucGFyc2UocmVzcG9uc2VKc29uLklWKS5pdmUpKSlcbiAgICAgICAgICAgICAgICAgICAgICAgIFdpbmRvdy5lUHJpdmF0ZUtleSA9IGF3YWl0IGNyeXB0LmdldEVQcml2YXRlS2V5RnJvbUJ1ZmZlcihhd2FpdCBjcnlwdC5kZWNyeXB0S2V5KHBhc3N3b3JkLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uZVByaXZhdGVLZXkpLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uSVYuSVZFKSkpXG4gICAgICAgICAgICAgICAgICAgICAgICBXaW5kb3cuc1ByaXZhdGVLZXkgPSBhd2FpdCBjcnlwdC5nZXRTUHJpdmF0ZUtleUZyb21CdWZmZXIoYXdhaXQgY3J5cHQuZGVjcnlwdEtleShwYXNzd29yZCxoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIocmVzcG9uc2VKc29uLnNQcml2YXRlS2V5KSxoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIocmVzcG9uc2VKc29uLklWLklWUykpKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2luZG93LmVQdWJsaWNLZXkgPSBhd2FpdCBjcnlwdC5nZXRFUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uZVB1YmxpY0tleSkpKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2luZG93LnNQdWJsaWNLZXkgPSBhd2FpdCBjcnlwdC5nZXRTUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uc1B1YmxpY0tleSkpKSAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coV2luZG93LmVQcml2YXRlS2V5KVxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coV2luZG93LnNQcml2YXRlS2V5KVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coV2luZG93LnNQdWJsaWNLZXkpXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhXaW5kb3cuZVB1YmxpY0tleSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coaGVscGVycy5jb252ZXJ0QXJyYXlCdWZmZXJ0b1N0cmluZyhhd2FpdCBjcnlwdC5kZWNyeXB0S2V5KHBhc3N3b3JkLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uc1B1YmxpY0tleSksaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKEpTT04ucGFyc2UocmVzcG9uc2VKc29uLklWKS5pdnNwKSkpKVxuICAgICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGdldHRpbmcgcHJpdmF0ZSBrZXlzOiAnK2UpXG4gICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgdXBsb2FkRmlsZShmaWxlLCBjcmVhdG9yRW1haWxJZCwgb3duZXJFbWFpbElkLCBpc0lkZW50aXR5LCBub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0LGlwZnNOb2RlSXAgPSB0aGlzLmxvY2FsSXBmc0lwLGlwZnNQb3J0ID0gdGhpcy5sb2NhbElwZnNQb3J0KSB7XG4gICAgICAgICAgICAvLzE6R2VuZXJhdGUgUmFuZG9tIEFlcyBLZXkgYW5kIGl0cyBwYXJhbXNcbiAgICAgICAgICAgIGxldCBhZXNLZXkgPSBhd2FpdCBjcnlwdC5nZW5lcmF0ZV9BRVNfS2V5KClcbiAgICAgICAgICAgIGxldCBpdiA9IHdpbmRvdy5jcnlwdG8uZ2V0UmFuZG9tVmFsdWVzKG5ldyBVaW50OEFycmF5KDE2KSlcbiAgICAgICAgICAgIC8vMjogR2V0IEhhc2hcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgaXBmc05vZGVJcCArIFwiOlwiK2lwZnNQb3J0K1wiL2FwaS92MC9ibG9jay9wdXRcIiBcbiAgICAgICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpXG4gICAgICAgICAgICAvL2ZldGNoIGRvZXNudCB3b3JrIHdpdGggbG9jYWwgZmlsZXMgc28gSSB1c2VkIHJlYWRlciBcbiAgICAgICAgICAgIHJlYWRlci5hZGRFdmVudExpc3RlbmVyKFwibG9hZFwiLCBhc3luYyAocmVzdWx0KSA9PiB7XG4gICAgICAgICAgICAgICAgdmFyIGZpbGVTdHJlYW0gPSBuZXcgRm9ybURhdGEoKSBcbiAgICAgICAgICAgICAgICAvLzM6IEVuY3J5cHQgdGhlIGZpbGUgd2l0aCBBZXMgS2V5XG4gICAgICAgICAgICAgICAgdmFyIGVuY3J5cHRlZEZpbGUgPSBhd2FpdCBjcnlwdC5lbmNyeXB0RmlsZUFlcyhyZWFkZXIucmVzdWx0LCBhZXNLZXksIGl2KS8vYWRkIGl2XG4gICAgICAgICAgICAgICAgZmlsZVN0cmVhbS5hcHBlbmQoJ3BhdGgnLCAobmV3IEJsb2IoWyhlbmNyeXB0ZWRGaWxlKV0pKSlcbiAgICAgICAgICAgICAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGZpbGVTdHJlYW0sXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocilcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBoYXNoID0gci5qc29uKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXN5bmMgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vMzogQWRkQmxvY2sgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmFkZEJsb2NrKHJlc3BvbnNlLktleSwgZmlsZS5uYW1lLCBmaWxlLnR5cGUsIGNyZWF0b3JFbWFpbElkLCBvd25lckVtYWlsSWQsIGFlc0tleSwgaXYsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzSWRlbnRpdHk9PXRydWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMudXBkYXRlSWRlbnRpdHkocmVzcG9uc2UuS2V5LCBvd25lckVtYWlsSWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHJlYWRlci5yZWFkQXNBcnJheUJ1ZmZlcihmaWxlKVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIHVwbG9hZEZpbGVzKGZpbGVMaXN0LCBjcmVhdG9yRW1haWxJZCwgb3duZXJFbWFpbElkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAgLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0LGlwZnNOb2RlSXAgPSB0aGlzLmxvY2FsSXBmc0lwLGlwZnNQb3J0ID0gdGhpcy5sb2NhbElwZnNQb3J0KSB7XG4gICAgICAgICAgICAvL2ZvciBsb29wIGZvciBtdWx0aXBsZSBmaWxlc1xuICAgICAgICAgICAgZmlsZUxpc3QuZm9yRWFjaChmaWxlID0+IHRoaXMudXBsb2FkRmlsZShmaWxlLCBjcmVhdG9yRW1haWxJZCwgb3duZXJFbWFpbElkLCBmYWxzZSxub2RlSXAsbm9kZVBvcnQsaXBmc05vZGVJcCxpcGZzUG9ydCkpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgYWRkQmxvY2soaGFzaCwgbmFtZSwgZm9ybWF0LCBjcmVhdG9yRW1haWxJZCwgb3duZXJFbWFpbElkLCBhZXNLZXksIGl2LCBub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vbGV0IHB1YmxpY1JlY2VpdmVyS2V5ID0gYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydClcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGF3YWl0IHRoaXMuZ2V0UHVibGljS2V5KG93bmVyRW1haWxJZCxub2RlSXAsbm9kZVBvcnQpKVxuICAgICAgICAgICAgbGV0IG93bmVyS2V5QnVmZmVyID0gKGF3YWl0IGNyeXB0LmVuY3J5cHRBZXNLZXkoYXdhaXQgY3J5cHQuZXhwb3J0QWVzKGFlc0tleSksIGF3YWl0IHRoaXMuZ2V0UHVibGljS2V5KG93bmVyRW1haWxJZCxub2RlSXAsbm9kZVBvcnQpKSlcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKG93bmVyS2V5QnVmZmVyKVxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIitub2RlUG9ydCtcIi9ibG9ja1wiXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoW3tcbiAgICAgICAgICAgICAgICBpcGZzSGFzaDogaGFzaCxcbiAgICAgICAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICAgICAgIGZvcm1hdDogZm9ybWF0LFxuICAgICAgICAgICAgICAgIHNpZ25hdG9yeUVtYWlsSWQ6IGNyZWF0b3JFbWFpbElkLFxuICAgICAgICAgICAgICAgIG93bmVyRW1haWxJZDogb3duZXJFbWFpbElkLFxuICAgICAgICAgICAgICAgIHBlcm1pc3Npb25zOiBbXG4gICAgICAgICAgICAgICAgICAgIHsgcmVjZWl2ZXJFbWFpbElkOiBvd25lckVtYWlsSWQsIHJlY2VpdmVyS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQob3duZXJLZXlCdWZmZXIpIH1cblxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgSVY6aGVscGVycy5hcnJheVRvQmFzZTY0KGl2KVxuICAgICAgICAgICAgfV0pXG4gICAgICAgICAgICAvLyBHZW5lcmF0ZSBTaWduYXR1cmVcbiAgICAgICAgICAgIGxldCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSlcbiAgICAgICAgICAgIGNvbnN0IGJsb2NrZGF0YSA9XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmV0Y2godXJsLCB7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShibG9ja2RhdGEpXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKClcbiAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgdXBkYXRlSWRlbnRpdHkoaGFzaCwgb3duZXJFbWFpbElkLCBub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vbGV0IHB1YmxpY1JlY2VpdmVyS2V5ID0gYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydClcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGF3YWl0IHRoaXMuZ2V0UHVibGljS2V5KG93bmVyRW1haWxJZCxub2RlSXAsbm9kZVBvcnQpKVxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIitub2RlUG9ydCtcIi91c2VyL2lkZW50aXR5XCJcbiAgICAgICAgICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgaWRlbnRpdHlGaWxlSGFzaDogaGFzaCxcbiAgICAgICAgICAgICAgICBlbWFpbElkOiBvd25lckVtYWlsSWRcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAvLyBHZW5lcmF0ZSBTaWduYXR1cmVcbiAgICAgICAgICAgIGxldCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSlcbiAgICAgICAgICAgIGNvbnN0IGJsb2NrZGF0YSA9XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmV0Y2godXJsLCB7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShibG9ja2RhdGEpXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKClcbiAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgIH1cblxuICAgICAgICBhc3luYyBnZXRCbG9jayhoYXNoLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpLy93aW5kb3cgb2JqZWN0IGVycm9yXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiK25vZGVJcCtcIjpcIitub2RlUG9ydCtcIi9ibG9jay9cIitoYXNoXG4gICAgICAgICAgICB2YXIgYmxvY2sgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgY29uc3QgcmVzcG9uc2VKc29uID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZUpzb247XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKCAoZSk9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIHdoaWxlIGZldGNoaW5nIGJsb2NrIGZyb20gYmFja2VuZCA6XCIrZSlcbiAgICAgICAgICAgICAgICByZXR1cm59KVxuICAgICAgICAgICAgcmV0dXJuIGJsb2NrXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgZG93bmxvYWRCbG9jayAoaGFzaCxlbWFpbElkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQsaXBmc05vZGVJcCA9IHRoaXMubG9jYWxJcGZzSXAsaXBmc1BvcnQgPSB0aGlzLmxvY2FsSXBmc1BvcnQpIFxuICAgICAgICB7XG4gICAgICAgICAgICBsZXQgYmxvY2tEYXRhID0gYXdhaXQgdGhpcy5nZXRCbG9jayhoYXNoLG5vZGVJcCxub2RlUG9ydClcbiAgICAgICAgICAgIC8vY2hlY2sgcGVybWlzc2lvbnNcbiAgICAgICAgICAgIGxldCByZWNlaXZlcktleVNcbiAgICAgICAgICAgIGZvciAobGV0IGluZGV4PTA7IGluZGV4IDwgYmxvY2tEYXRhLnBlcm1pc3Npb25zLmxlbmd0aDsgaW5kZXgrKylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihibG9ja0RhdGEucGVybWlzc2lvbnNbaW5kZXhdLnJlY2VpdmVyRW1haWxJZD09IGVtYWlsSWQpe1xuICAgICAgICAgICAgICAgICAgICByZWNlaXZlcktleVM9IGJsb2NrRGF0YS5wZXJtaXNzaW9uc1tpbmRleF0ucmVjZWl2ZXJLZXlcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0cnkgXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICBpZiAocmVjZWl2ZXJLZXlTID09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IFBlcm1pc3Npb25FcnJvcigpXG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgY2F0Y2goZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZS5tZXNzYWdlKVxuICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgXG4gICAgICAgICAgICBsZXQgcmVjZWl2ZXJLZXlCID0gaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKHJlY2VpdmVyS2V5UylcbiAgICAgICAgICAgIC8vbG9naW4gYmVmb3JlIHlvdSBjYWxsIHRoaXMgbWV0aG9kXG4gICAgICAgICAgICBsZXQgYWVzS2V5QnVmZmVyID0gYXdhaXQgY3J5cHQuZGVjcnlwdEFlc0tleShyZWNlaXZlcktleUIsV2luZG93LmVQcml2YXRlS2V5KVxuICAgICAgICAgICAgbGV0IGFlc0tleSA9IGF3YWl0IGNyeXB0LmltcG9ydEFlcyhhZXNLZXlCdWZmZXIpXG4gICAgICAgICAgICAvL2dldCBmaWxlIGJ1ZmZlciBhbmQgZGVjcnlwdCB3aXRoIGFlcyBrZXlcbiAgICAgICAgICAgIGxldCBpdiA9IGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihibG9ja0RhdGEuSVYpXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIitpcGZzTm9kZUlwK1wiOlwiK2lwZnNQb3J0K1wiL2FwaS92MC9ibG9jay9nZXQ/YXJnPVwiK2hhc2hcbiAgICAgICAgICAgIHZhciBmaWxlID0gYXdhaXQgZmV0Y2goIHVybCwge1xuICAgICAgICAgICAgICAgIG1ldGhvZDpcIlBPU1RcIixcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpPT4gXG4gICAgICAgICAgICB7Ly9jYW50IGNvbnZlcnQgdG8ganNvblxuICAgICAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgICAgICByZXR1cm4gYXdhaXQgcmVzcG9uc2UuYXJyYXlCdWZmZXIoKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICBhc3luYyAoZW5jcnlwdGVkRmlsZUJ1ZmZlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGZpbGUgPSBuZXcgRmlsZShbYXdhaXQgY3J5cHQuZGVjcnlwdEZpbGVBZXMoZW5jcnlwdGVkRmlsZUJ1ZmZlcixhZXNLZXksaXYpXSxibG9ja0RhdGEubmFtZSx7dHlwZTogYmxvY2tEYXRhLmZvcm1hdH0pXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmUoZmlsZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmaWxlXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICB9KVx0XG4gICAgICAgICAgICAuY2F0Y2goKGUpPT5cbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igb2NjdXJlZCB3aGlsZSBkb3dubG9hZGluZyBibG9jazogXCIrZSlcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgc2F2ZShmaWxlKSB7XG4gICAgICAgICAgICAvL3ZhciBibG9iID0gbmV3IEJsb2IoW2RhdGFdLCB7dHlwZTp0eXBlIH0pIFxuICAgICAgICAgICAgaWYod2luZG93Lm5hdmlnYXRvci5tc1NhdmVPck9wZW5CbG9iKSB7XG4gICAgICAgICAgICAgICAgd2luZG93Lm5hdmlnYXRvci5tc1NhdmVCbG9iKGZpbGUsIGZpbGUubmFtZSkgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgIHZhciBlbGVtID0gd2luZG93LmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKSBcbiAgICAgICAgICAgICAgICBlbGVtLmhyZWYgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChmaWxlKSBcbiAgICAgICAgICAgICAgICBlbGVtLmRvd25sb2FkID0gZmlsZS5uYW1lICAgICAgICAgXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChlbGVtKSBcbiAgICAgICAgICAgICAgICBlbGVtLmNsaWNrKCkgICAgICAgICBcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKGVsZW0pIFxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAvL05vdGUgdGhhdCwgZGVwZW5kaW5nIG9uIHlvdXIgc2l0dWF0aW9uLCB5b3UgbWF5IGFsc28gd2FudCB0byBjYWxsIFVSTC5yZXZva2VPYmplY3RVUkwgYWZ0ZXIgcmVtb3ZpbmcgZWxlbS4gXG4gICAgICAgIC8vQWNjb3JkaW5nIHRvIHRoZSBkb2NzIGZvciBVUkwuY3JlYXRlT2JqZWN0VVJMOlxuICAgICAgICBcbiAgICAgICAgLy9FYWNoIHRpbWUgeW91IGNhbGwgY3JlYXRlT2JqZWN0VVJMKCksIGEgbmV3IG9iamVjdCBVUkwgaXMgY3JlYXRlZCwgXG4gICAgICAgIC8vZXZlbiBpZiB5b3UndmUgYWxyZWFkeSBjcmVhdGVkIG9uZSBmb3IgdGhlIHNhbWUgb2JqZWN0LiBFYWNoIG9mIHRoZXNlIG11c3QgYmUgcmVsZWFzZWQgYnkgY2FsbGluZyBVUkwucmV2b2tlT2JqZWN0VVJMKCkgXG4gICAgICAgIC8vd2hlbiB5b3Ugbm8gbG9uZ2VyIG5lZWQgdGhlbS4gQnJvd3NlcnMgd2lsbCByZWxlYXNlIHRoZXNlIGF1dG9tYXRpY2FsbHkgd2hlbiB0aGUgZG9jdW1lbnQgaXMgdW5sb2FkZWQgIFxuICAgICAgICAvL2hvd2V2ZXIsIGZvciBvcHRpbWFsIHBlcmZvcm1hbmNlIGFuZCBtZW1vcnkgdXNhZ2UsIGlmIHRoZXJlIGFyZSBzYWZlIHRpbWVzIHdoZW4geW91IGNhbiBleHBsaWNpdGx5IHVubG9hZCB0aGVtLCBcbiAgICAgICAgLy95b3Ugc2hvdWxkIGRvIHNvLlxuXG4gICAgICAgIGFzeW5jIGxpc3RCbG9jayAobm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0LG93bmVyRW1haWxJZCxwZXJtaXR0ZWRFbWFpbElkKVxuICAgICAgICB7XG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIitub2RlSXArXCI6XCIrbm9kZVBvcnQrXCIvYmxvY2tzXCJcbiAgICAgICAgICAgIHZhciBib2R5ID0ge31cbiAgICAgICAgICAgIGlmIChvd25lckVtYWlsSWQhPXVuZGVmaW5lZCAmJiBwZXJtaXR0ZWRFbWFpbElkIT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGJvZHkub3duZXJFbWFpbElkPSBhcmd1bWVudHNbMl1cbiAgICAgICAgICAgICAgICBib2R5LnBlcm1pdHRlZEVtYWlsSWQ9IGFyZ3VtZW50c1szXVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHBlcm1pdHRlZEVtYWlsSWQ9PXVuZGVmaW5lZClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBib2R5Lm93bmVyRW1haWxJZD0gYXJndW1lbnRzWzJdXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob3duZXJFbWFpbElkPT11bmRlZmluZWQpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgYm9keS5wZXJtaXR0ZWRFbWFpbElkPSBwZXJtaXR0ZWRFbWFpbElkXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob3duZXJFbWFpbElkPT11bmRlZmluZWQgJiYgcGVybWl0dGVkRW1haWxJZD09dW5kZWZpbmVkKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGJvZHkub3duZXJFbWFpbElkPSBcIlwiXG4gICAgICAgICAgICAgICAgYm9keS5wZXJtaXR0ZWRFbWFpbElkPSBcIlwiXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmZXRjaCh1cmwsIHtcbiAgICAgICAgICAgICAgICBtZXRob2Q6XCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgYm9keTpKU09OLnN0cmluZ2lmeShib2R5KVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4gaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKSlcbiAgICAgICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXG4gICAgICAgICAgICAgICAgcmV0dXJufSlcbiAgICAgICAgfVxuICAgICAgICBhc3luYyBsb2dvdXQgKClcbiAgICAgICAge1xuICAgICAgICAgICAgLy9kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY3VycmVudF91c2VyJykuaW5uZXJIVE1MPSBcIlwiXG4gICAgICAgICAgICBkZWxldGUgV2luZG93Lm1iVXNlclxuICAgICAgICAgICAgZGVsZXRlIFdpbmRvdy5lUHVibGljS2V5XG4gICAgICAgICAgICBkZWxldGUgV2luZG93LnNQdWJsaWNLZXlcbiAgICAgICAgICAgIGRlbGV0ZSBXaW5kb3cuZVByaXZhdGVLZXlcbiAgICAgICAgICAgIGRlbGV0ZSBXaW5kb3cuc1ByaXZhdGVLZXlcbiAgICAgICAgfVxuICAgICAgICAvL0Fzc2V0IGZ1bmN0aW9uc1xuXG4gICAgICAgIGFzeW5jIG5ld0Fzc2V0KGFzc2V0TmFtZSxhc3NldEFkbWluPVdpbmRvdy5tYlVzZXIsYXNzZXRNaW50ZXJzLGluaXRCYWxhbmNlLG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcbiAgICAgICAge1xuICAgICAgICAgICAgYXNzZXQubmV3QXNzZXQoYXNzZXROYW1lLGFzc2V0QWRtaW4sYXNzZXRNaW50ZXJzLGluaXRCYWxhbmNlLG5vZGVJcCxub2RlUG9ydClcbiAgICAgICAgfVxuICAgICAgICBhc3luYyBnZXRNaW50ZXJzIChhc3NldE5hbWUsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KSB7XG4gICAgICAgICAgICBhc3NldC5nZXRNaW50ZXJzKGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgY2hhbmdlQWRtaW4oYXNzZXROYW1lLGFzc2V0QWRtaW49dGhpcy5tYlVzZXIsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxuICAgICAgICB7XG4gICAgICAgICAgICBhc3NldC5jaGFuZ2VBZG1pbihhc3NldE5hbWUsYXNzZXRBZG1pbixub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgYnVybiAoYXNzZXROYW1lLGFtb3VudCxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFzc2V0LmJ1cm4oYXNzZXROYW1lLGFtb3VudCxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgdG90YWxTdXBwbHkgIChhc3NldE5hbWUsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxuICAgICAgICB7XG4gICAgICAgICAgICBhc3NldC50b3RhbFN1cHBseShhc3NldE5hbWUsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIHRyYW5zZmVyIChhc3NldE5hbWUsZnJvbT10aGlzLm1iVXNlcix0byxhbW91bnQsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxuICAgICAgICB7XG4gICAgICAgICAgICBhc3NldC50cmFuc2Zlcihhc3NldE5hbWUsZnJvbSx0byxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIHRyYW5zZmVyVG9BZG1pbiAgKGFzc2V0TmFtZSxmcm9tLGFtb3VudCxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFzc2V0LnRyYW5zZmVyVG9BZG1pbihhc3NldE5hbWUsZnJvbSxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIHRyYW5zZmVyRnJvbUFkbWluICAoYXNzZXROYW1lLHRvLGFtb3VudCxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFzc2V0LnRyYW5zZmVyRnJvbUFkbWluKGFzc2V0TmFtZSx0byxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIGFkZE1pbnRlcnMgKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxuICAgICAgICB7XG4gICAgICAgICAgICBhc3NldC5hZGRNaW50ZXJzKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIHJlbW92ZU1pbnRlcnMgKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxuICAgICAgICB7XG4gICAgICAgICAgICBhc3NldC5yZW1vdmVNaW50ZXJzKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG4gICAgICAgIGFzeW5jIG1pbnQgKGFzc2V0TmFtZSx0YXJnZXQsYW1vdW50LG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcbiAgICAgICAge1x0XG5cdFx0dmFyIG1pbnRlcj1XaW5kb3cubWJVc2VyXG4gICAgICAgICAgICBhc3NldC5taW50KGFzc2V0TmFtZSxtaW50ZXIsdGFyZ2V0LGFtb3VudCxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgY2hlY2tBZG1pbkJhbGFuY2UoYXNzZXROYW1lLG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PSB0aGlzLmxvY2FsTm9kZVBvcnQpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFzc2V0LmNoZWNrQWRtaW5CYWxhbmNlKGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpXG4gICAgICAgIH1cbiAgICAgICAgYXN5bmMgY2hlY2tCYWxhbmNlIChhc3NldE5hbWUsdGFyZ2V0LG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcbiAgICAgICAge1xuICAgICAgICAgICAgYXNzZXQuY2hlY2tCYWxhbmNlKGFzc2V0TmFtZSx0YXJnZXQsbm9kZUlwLG5vZGVQb3J0KVxuICAgICAgICB9XG5cbiAgICB9XG4gICAgd2luZG93Lk1lZGJsb2NrcyA9IE1lZGJsb2NrcyBcbn0pKCkgXG4iXSwic291cmNlUm9vdCI6IiJ9