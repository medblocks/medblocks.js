FROM atlassianlabs/docker-node-jdk-chrome-firefox
WORKDIR /src
ADD ./package.json /src/package.json
RUN npm install
ADD ./ /src/
RUN npm run-script build
CMD ["npm", "test"]